require "csv"

# PIXEL CAMPAIGNS


Campaign_Status = 'ACTIVE'
NEWUSED = ["new", "!"]
DISPLAY = "Subaru.SmartAutoSavings.com"
DESTINATION = "http://pixel.autoaffiliatenetwork.com/d/?dest=SAS"

def ad(title, description)
  { "title" => title, "description" => description }
end



ADS = [
    ad("Subaru 2016 Clearance Prices!", "Find amazing deals on 2015 Subaru. Click here to browse and compare offers!"),
    ad("This is the ad_title2", "This is the ad_copy2")
]



#&adtargeting=30-120_day&campaign=pixel&device=d&id=527&dest=SAS&admake=Subaru&creativeid=6035594482900&account=sas_abtl

#Pixel ID: 1182339951781398

#Ad ID: 6035594482900


csv_string = CSV.generate do |csv|
  csv << ["Ad status","Final URL","Headline 1","Headline 2","Headline 3","Description",
    "Description 2","Tracking template","Path 1","Path 2","Campaign","Ad group"]
  DATA.each_line do |line|
    make, model = line.strip.split(",")
    NEWUSED.each do |newused|
      ADS.each do |ad|
        csv << [Campaign_Status, newused, make, model, ad['title'], ad['description'], DISPLAY, DESTINATION]
      end
    end
  end
end

puts csv_string

__END__
audi,a1
audi,a1-sportback
audi,a3
audi,a3-cabriolet
audi,a3-sportback
audi,a4
audi,a4-allroad
audi,a4-avant
audi,a5-coupe
audi,a5-sportback
audi,a6
audi,a6-avant
audi,a7-sportback
audi,a8
audi,q2
audi,q3
audi,q5
audi,q7
audi,q8
audi,rs3-sportback
audi,s3
audi,s3-sportback
audi,tt-coupe
audi,tt-roadster
kia,carens
kia,ceed-sw
kia,optima
kia,picanto
kia,ql
kia,rio
kia,soul
kia,sportage
kia,stinger
kia,stonic
kia,venga
skoda,citigo
skoda,fabia
skoda,fabia-combi
skoda,karoq
skoda,kodiaq
skoda,octavia
skoda,octavia-combi
skoda,superb
volkswagen,arteon
volkswagen,golf
volkswagen,golf-variant
volkswagen,maggiolino
volkswagen,passat
volkswagen,passat-variant
volkswagen,polo
volkswagen,sharan
volkswagen,t-roc
volkswagen,tiguan
volkswagen,touareg
volkswagen,touran
volkswagen,up
