#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 28 11:45:51 2019

@author: jovanna
"""
from itertools import chain
import requests
import sys
import os
sys.path.insert(0,os.environ['SDP_ROOT'])
from utils.OpenConn import DbConnection as Conn
import time
import pdb

query1 = """
SELECT cap, nome FROM facts.caps where longitude = 0.0;
"""
query2 = """
UPDATE facts.caps set latitude = {}, longitude = {} where cap = '{}'
"""
query3 = """
UPDATE facts.caps set latitude = {}, longitude = {} where cap = '{}' and nome = "{}"
"""

def get_data(db,query):
    cursor = db.cursor()
    cursor.execute(query)
    m = [(row[0], row[1]) for row in iter(cursor.fetchone, None)]
    return m

def push_data(db,query,inputs):
    cursor = db.cursor()
    print query.format(*inputs)
    cursor.execute(query.format(*inputs))
    db.commit()
    
def process_data_mapit(conn, id, nome):
    r = requests.get('http://mapit.openpolis.it/postcode/{}'.format(id))
    r_json = r.json()
    print id
    if 'error' not in r_json.keys():
        #print r_json.keys()
        lat = r_json["wgs84_lat"]
        lon = r_json["wgs84_lon"]
        print 'writing lat and lon'
        push_data(conn,query3,(lat,lon,id,nome))
        time.sleep(1)
    else:
        lat = 0 
        lon = 0 
        print 'skipping'
        push_data(conn,query2,(lat,lon,id))
        time.sleep(1)

def process_data_zippo(conn, id, nome):
    r = requests.get('http://api.zippopotam.us/it/{}'.format(id))
    r_json = r.json()
    print id
    if 'post code' in r_json.keys():
        places = r_json['places']
        for place in places:
            nome = place['place name']
            lat = place['latitude']
            lon = place['longitude']
            print 'writing lat and lon'
            push_data(conn,query3,(lat,lon,id,nome))
            time.sleep(1)
    else:
        lat = 0 
        lon = 0 
        print 'skipping'
        push_data(conn,query2,(lat,lon,id))
        time.sleep(1)

if __name__ == '__main__':
    with Conn('sdp1') as c:
        conn = c.open_db('opt')
        caps = get_data(conn,query1,)
        #cap = '00141'
        #process_data(cap)        
        [process_data_zippo(conn, id, nome) for id, nome in caps]
        print 'data uploaded'
        
