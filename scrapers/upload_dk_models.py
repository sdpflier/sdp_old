#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
import os
import re
import csv
import pymysql
import sshtunnel
import logging
from decimal import Decimal
import pdb
sys.path.insert(0,os.environ['SDP_ROOT'])


from utils.OpenConn import DbConnection as Conn

_drop_dk_models_table = \
  "drop table if exists dk_models;"

_create_dk_models_table = \
  "CREATE TABLE `dk_models` (\
  `id` bigint(20) NOT NULL AUTO_INCREMENT,\
  `name` varchar(255) DEFAULT NULL,\
  `model_url` varchar(255) DEFAULT NULL,\
  `make` varchar(255) DEFAULT NULL,\
  `price` varchar(255) DEFAULT NULL,\
  `img_name` varchar(255) DEFAULT NULL,\
  `versions` varchar(255) DEFAULT NULL,\
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,\
  `updated_at` datetime NOT NULL default current_timestamp on update current_timestamp,\
  `title` varchar(255) DEFAULT NULL,\
  `active` int(11) DEFAULT NULL,\
  `body_type` varchar(255) DEFAULT NULL,\
  PRIMARY KEY (`id`)\
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;"

def clean_model(model):
    #lower case everything
    model =  model.lower()
    #remove Nuova
    model = model.replace('nuovo','')
    #remove Nuovo
    model = model.replace('nuova','')
    #remove leading or trailing spaces
    model = model.lstrip().rstrip()
    #remove !+
    model = model.replace('+','')
    model = model.replace('!','')
    model = model.decode('utf-8').replace(u"é",'e').encode('utf-8')
    #spaces with -
    model = model.replace(' ','-')
    #multiple - with one -
    model = re.sub(r'[-]+','-',model)
    return model

def get_make(model, carsConn):
    carsCur = carsConn.cursor()
    carsCur.execute("select make_name from cars.models where name = '{0}';".format(model))
    makes = carsCur.fetchone()
    make = None
    if ((makes is not None) and (len(makes) > 0)):
        make = makes[0]
    return make;

def clean_price(price):
    price = price.decode('utf8').replace(u"€",'').encode('utf8')
    price = price.replace('da','')
    price = price.lstrip().rstrip()
    return price
    

def upload( optConn, carsConn ):
    logging.basicConfig(filename='upload_dk_models.log', filemode='w', level=logging.DEBUG)
    optCur = optConn.cursor()
    carsCur = carsConn.cursor()
    
    optCur.execute(_drop_dk_models_table)
    optCur.execute(_create_dk_models_table)
    #name,price,model_url,img_url,img_name,versions
    with open('models.csv','r') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                #print(f'Column names are {", ".join(row)}')
                line_count += 1
            else:
                model = clean_model(row[0])
                price = row[1]
                make = get_make(model, carsConn)
                price = clean_price(price)
                carsCur.execute('insert into optimization.dk_models (name, make, model_url, price, img_name, versions) values ("{0}","{1}","{2}","{3}","{4}", "{5}")'.format(model, make, row[2], price, row[4], row[5]))
                line_count += 1
        #print(f'Processed {line_count} lines.')

    

if __name__ == "__main__":
    with Conn('localhost') as c:
        optdb = c.open_db('opt')   
        carsdb = c.open_db('cars')   
        upload(optdb,carsdb)
        carsdb.commit()
