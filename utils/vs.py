#!/usr/bin/env python
# Script to graph one column vs another
# Usage: python vs.py --x comma,list,db.table.column -y db.table.column --connection conn
import argparse
import sys
import os
import datetime
import time
import pylab
import matplotlib
import itertools

from autotegrity import db

# -------- Form queries and get x/y data sets
def get_data(x_vals, y_vals, start_date=None, end_date=None, date_column='updated_at', and_clauses=[], limit=None, connection='odp'):
  x_vals = x_vals.split('.')
  x = {}
  x["db_name"] = x_vals[0]
  x["table_name"] = x_vals[1]
  x["column_name"] = x_vals[2]

  y_vals = y_vals.split('.')
  y = {}
  y["db_name"] = y_vals[0]
  y["table_name"] = y_vals[1]
  y["column_name"] = y_vals[2]

  where_clause = ""
  if start_date != None:
    where_clause += "where %s.%s >= '%s'" % (x["table_name"], date_column, start_date)
  if end_date != None:
    if start_date != None:
      where_clause += " and %s.%s <= '%s'" % (x["table_name"], date_column, end_date)
    else:
      where_clause += "where %s.%s <= '%s'" % (x["table_name"], date_column, end_date)

  and_clause = ''
  for clause in and_clauses:
    if x["table_name"] not in clause and y["table_name"] not in clause:
      print "skipping and_clause, must use same table(s) as x and/or y -> %s" % clause
      continue
    if '\\n' in clause:
      clause = clause.replace('\\n', '\n')
    if where_clause == "":
      # First make a where_clause
      where_clause = "where " + clause
    else:
      and_clause += (" and " + clause)

  limit_clause = ''
  if limit is not None:
    limit_clause = "limit %d" % limit

  if x["table_name"] != y["table_name"]:
    print "Table names differ - trying to join on x.%s" % x["column_name"]
    SQL = """
    select
    %s.%s, %s.%s
    from %s.%s join %s.%s on %s.%s = %s.%s
    """ % (x["table_name"], x["column_name"], y["table_name"], y["column_name"],
           x["db_name"], x["table_name"], y["db_name"], y["table_name"],
           x["table_name"], x["column_name"], y["table_name"], x["column_name"])
    SQL = SQL + where_clause + "\n" + and_clause + "\n" + limit_clause + ("    order by %s.%s asc" % (x["table_name"], date_column))
    SQL = SQL.replace("\n\n", "\n")

    conn = db.connection(connection, x["db_name"])
    cursor = conn.cursor()

    print "X/Y Query: %s" % SQL
    db.execute(cursor, SQL, query_name=__file__)
    x_out = []
    y_out = []
    vals = cursor.fetchall()
    for val in vals:
      x_out.append(val[0])
      y_out.append(val[1])

  else:
    # table names match
    SQL = """
     select
     %s.%s, %s.%s
     from %s
    """ % (x["table_name"], x["column_name"], y["table_name"], y["column_name"], x["table_name"])
    SQL = SQL + where_clause + "\n" + and_clause + "\n" + limit_clause + ("    order by %s asc" % date_column)
    SQL = SQL.replace("\n\n", "\n")
  
    conn = db.connection(connection, x["db_name"])
    cursor = conn.cursor()

    print "X/Y Query: %s\n" % SQL
    db.execute(cursor, SQL, query_name=__file__)
    x_out = []
    y_out = []
    vals = cursor.fetchall()
    for val in vals:
      x_out.append(val[0])
      y_out.append(val[1])

  conn.commit()
  cursor.close()
  conn.close()
  return x_out, y_out

# --------
def add_graph(x_data, y_data, xlabel, ylabel, title, date_interval=6, fmt='b.-', use_axis=None, num_axes=0):
  pylab.title(title)
  if use_axis is None:
    ax = pylab.gca()
    if num_axes > 0:
      ax = ax.twinx()
  else:
    ax = use_axis

  if "run_date" in xlabel or "created_at" in xlabel or "updated_at" in xlabel:
    x_time = []
    for x in x_data:
      timeval = str(x).split('.')[0]
      x_time.append(timeval)
    ax.plot_date(pylab.date2num(x_data), y_data, fmt=fmt, xdate=True, label=ylabel)
    ax.tick_params('x', labelsize='xx-small')
    loc = matplotlib.dates.HourLocator(interval=date_interval)
    ax.xaxis.set_major_locator(loc)
    ax.xaxis.set_major_formatter(matplotlib.dates.DateFormatter('%H:%M\n%m/%d'))
  else:
    ax.plot(x_data, y_data, fmt, label=ylabel)

  return ax


# --------
if __name__ == "__main__":
  def date(s):
    try:
      d = datetime.datetime.strptime(s, "%Y-%m-%d %H:%M")
    except:
      d = datetime.datetime.strptime(s, "%Y-%m-%d").date()
    return d

  # XXX should be able to specify interval size, which will translate into windows that get
  # averaged or something -- just need a way to specify how the data is sampled

  parser = argparse.ArgumentParser()
  parser.add_argument('--start-date', help='Start date e.g. 2010-10-01 10:00', type=date, default=datetime.datetime.now() - datetime.timedelta(hours=6))
  parser.add_argument('--end-date',   help='End date e.g. 2010-10-01 11:00', type=date, default=datetime.datetime.now())
  parser.add_argument('--date-column', help='range by a specific date column', type=str, default='updated_at')
  parser.add_argument('--fmt', help='format string for plot', type=str, default='.-')
  parser.add_argument('--figwidth', help='multiplier for figure width', type=int, default=8)
  parser.add_argument('--limit', help='limit on number of rows in each table', type=int, default=None)
  parser.add_argument('--connection', help='database connection name', default="odp")
  parser.add_argument('--title', help='Title for figure', default="")
  parser.add_argument('--x', help='Comma-seperated list of db.table.columns for x axis', default="")
  parser.add_argument('--y', help='Comma-seperated list of db.table.columns for y axis', default="")
  parser.add_argument('--date-interval', help='interval in hours for the graph', type=int, default=6)
  parser.add_argument('--and-clause', help='specify an \'and\' clause, can be comma separated strings', type=str, default='')
  
  args = parser.parse_args()

  clauses=[]
  if args.and_clause != '':
    clauses = args.and_clause.split(',')

  print "Using date range: %s to %s" % (args.start_date, args.end_date)

  x_axes = args.x.split(',')
  y_axes = args.y.split(',')

  if args.title == '':
    title = "%s --> %s" % (str(args.start_date).split('.')[0], str(args.end_date).split('.')[0])
  else:
    title = args.title

  colors = [x + args.fmt for x in ['b', 'g', 'r', 'c', 'm', 'y', 'k']]
  colors = itertools.cycle(colors)

  pylab.figure(1, figsize=(args.figwidth*len(x_axes),8))
  for x_axis in x_axes:
    plotnum = x_axes.index(x_axis) + 1
    print "\n--- New Subplot - 1,%d,%d" % (len(x_axes), plotnum)
    ax = pylab.subplot(1, len(x_axes), plotnum)
    x_out = []; x_old = []
    y_out = []; y_old = []
    axes = {}
    new_axis = False

    for y_axis in y_axes:
      if len(x_out) != 0: x_old = x_out[:]
      if len(y_out) != 0: y_old = y_out[:]
      x_out, y_out = get_data(x_axis, y_axis, start_date=args.start_date, end_date=args.end_date, date_column=args.date_column, and_clauses=clauses, limit=args.limit, connection=args.connection)
      x_name = x_axis.split('.')[-1]
      y_name = y_axis.split('.')[-1]

      y_max = max(y_out)
      use_axis = None
      if len(axes) > 0:
        for a in axes:
          # if y fits within this bounds, re-use axis
          if y_max < 5*axes[a]["max"] and 5*y_max > axes[a]["max"]:
            use_axis = axes[a]["axis"]
          break
      
      axis = add_graph(x_out, y_out, x_name, y_name, title, date_interval=args.date_interval, fmt=colors.next(), use_axis=use_axis, num_axes=len(axes))
      key = axis.__hash__()
      if use_axis is None:
        axes[key] = {}
        axes[key]["axis"] = axis
        axes[key]["max"] = y_max
        axes[key]["name"] = [y_name]
        axes[key]["num"] = len(axes)
      else:
        axes[key]["max"] = y_max
        axes[key]["name"].append(y_name)


    pylab.xlabel(x_name)
    pylab.grid(True)
    for a in axes:
      axes[a]["axis"].legend(loc=len(axes)+1-axes[a]["num"], ncol=1)

  pylab.savefig('tmp_vs.png')
  os.system('open tmp_vs.png')

