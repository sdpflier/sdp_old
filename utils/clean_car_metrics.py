#!/usr/bin/python
import sys
import os
import re
import pymysql
import sshtunnel
import logging
import pdb
sys.path.insert(0,os.environ['SDP_ROOT'])

from utils.OpenConn import DbConnection as Conn

_drop_clean_car_table = \
  "drop table if exists clean_car_metrics;"

_create_clean_car_table = \
   "CREATE TABLE `clean_car_metrics` (\
  `id` int(11) NOT NULL AUTO_INCREMENT,\
  `make_name` varchar(45) DEFAULT NULL,\
  `model__name` varchar(60) DEFAULT NULL,\
  `body_type` varchar(60) DEFAULT NULL,\
  `name` varchar(60) DEFAULT NULL,\
  `value` varchar(60) DEFAULT NULL, \
  `cleaned_at` datetime DEFAULT NULL,\
  `created_at` datetime DEFAULT NULL,\
  `updated_at` datetime DEFAULT NULL,\
  `deleted_at` datetime DEFAULT NULL,\
  PRIMARY KEY (`id`)\
) ENGINE=InnoDB DEFAULT CHARSET=utf8"

_valid_fields = ['id','make_name','model__name',\
                     'body_type','body_kind',\
                     'name','value',\
                     'updated_at','deleted_at',\
                     'cleaned_at','created_at']


_insert_to_clean_car_table =\
    "insert into clean_car_metrics (make_name, model__name, body_type, name, value, created_at) values ('{0}', '{1}', '{2}', '{3}', '{4}', current_timestamp())"

def remove_nulls_and_execute(sql, carsCur):
    sql = sql.replace("'NULL'",'NULL')
    carsCur.execute(sql)
    
# Simple routine to run a query on a database and print the results:
def main( carsConn, konConn ) :
    logging.basicConfig(filename='clean_car_metrics.log', filemode='w', level=logging.DEBUG)
    carsCur = carsConn.cursor()
    konCur = konConn.cursor()
    
    carsCur.execute(_drop_clean_car_table)
    carsCur.execute(_create_clean_car_table)

    carsCur.execute("select model__name, make_name, body_type, count(*) from clean_cars group by 1,2,3;")
    for metric in carsCur.fetchall():
        remove_nulls_and_execute(_insert_to_clean_car_table.format(metric[1], metric[0], metric[2],'count', metric[3]), carsCur)

    carsCur.execute("select model__name, make_name, body_type, min(base_price_cents), max(base_price_cents), avg(base_price_cents) from clean_cars group by 1,2,3;")
    for metric in carsCur.fetchall():
        remove_nulls_and_execute(_insert_to_clean_car_table.format(metric[1], metric[0], metric[2],'price_range', str(metric[3]) + '-' + str(metric[4])), carsCur)
        remove_nulls_and_execute(_insert_to_clean_car_table.format(metric[1], metric[0], metric[2],'average_price', str(metric[5])), carsCur)

if __name__ == "__main__":
    #To switch to local just enter localhost in Conn('localhost')
    with Conn('sdp1') as c:
        carsdb = c.open_db('cars')   
        konfdb = c.open_db('kond')
        main(carsdb, konfdb)
        carsdb.commit()
