#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 15 13:40:48 2018

@author: jovanna
"""

from mail import Email

m = Email()
mFrom = "SDP Report <notifications@groupsdp.com>"
mTo = "jovanna.selvaggi@groupsdp.com"
m.setFrom(mFrom)
m.addRecipient(mTo)
m.setHtmlBody("The following should be <b>bold</b>")
m.send()


# =============================================================================
# # Simple Plain Text Email
# m.setSubject("Plain text email")
# m.setTextBody("This is a plain text email <b>I should not be bold</b>")
# m.send()
# 
# # Plain text + attachment
# m.setSubject("Text plus attachment")
# m.addAttachment("/Users/jovanna/Desktop/Vieste.jpg")
# m.send()
# 
# # Simple HTML Email
# m.clearAttachments()
# m.setSubject("HTML Email")
# m.setTextBody(None)
# m.setHtmlBody("The following should be <b>bold</b>")
# m.send()
# 
# # HTML + attachment
# m.setSubject("HTML plus attachment")
# m.addAttachment("/Users/jovanna/Desktop/Vieste.jpg")
# m.send()
# 
# # Text + HTML
# m.clearAttachments()
# m.setSubject("Text and HTML Message")
# m.setTextBody("You should not see this text in a MIME aware reader")
# m.send()
# 
# # Text + HTML + attachment
# m.setSubject("HTML + Text + attachment")
# m.addAttachment("/Users/jovanna/Desktop/Vieste.jpg")
# m.send()
# 
# =============================================================================
print('Email is on the way')
