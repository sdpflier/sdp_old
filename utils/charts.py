import matplotlib
matplotlib.use("Agg")
from math import pi
from numpy import array, arange, sin
import pylab as P
import itertools
import matplotlib.font_manager
import os

def clustered_columns(series, categories, **kwargs):
  """
      series = {
        "May" : {
          "Ford": 1,
          "Chevrolet": 2
        },
        "June" : {
          "Ford": 5,
          "Mazda": 7
        }
      }
      
      categories = ["Ford", "Chevrolet", "Mazda"]
  """
  colors = itertools.cycle(['b', 'g', 'r', 'c', 'm', 'y', 'k'])
  
  # Location of X bars
  ind = arange(len(categories))

  # Width of bars
  width = 0.35

  fig = P.figure(figsize=(8,6))
  ax = fig.add_subplot(111)
  ax.grid(True)
  offset = -width
  
  all_bars = []
  all_legend = []
  
  categories.sort()
  names = series.keys()
  names.sort()
  
  for name in names:
    data = series[name]
    color = colors.next()
    offset += width
    new_data = []
    for category in categories:
      new_data.append(data.get(category, 0.0))
    bars = ax.bar(ind+offset, new_data, width, color=color)
    
    all_bars.append(bars[0])
    all_legend.append(name)
    
  ax.set_xticks(ind+offset)
  ax.set_xticklabels(categories, rotation="vertical")

  prop = matplotlib.font_manager.FontProperties(size=8)
  ax.legend(all_bars, all_legend, loc=kwargs.get("legend_loc", "right"))
  
  if "title" in kwargs:   ax.set_title(kwargs["title"])
  if "x_label" in kwargs: ax.set_xlabel(kwargs["x_label"])
  if "y_label" in kwargs: ax.set_ylabel(kwargs["y_label"])
  if "y_lim" in kwargs:   ax.set_ylim(kwargs["y_lim"])
 
  return fig

def stacked_columns(series, categories, **kwargs):
  """
      series = {
        "May" : {
          "Ford": 1,
          "Chevrolet": 2
        },
        "June" : {
          "Ford": 5,
          "Mazda": 7
        }
      }

      categories = ["Ford", "Chevrolet", "Mazda"]
  """
  colors = itertools.cycle(['b', 'g', 'r', 'c', 'm', 'y', 'k'])

  # Location of X bars
  ind = arange(len(categories))

  # Width of bars
  width = 0.35

  fig = P.figure(figsize=(8,6))
  ax = fig.add_subplot(111)
  ax.grid(True)

  all_bars = []
  all_legend = []

  categories.sort()
  names = series.keys()
  names.sort()

  last_data = None
  
  for name in names:
    data = series[name]
    color = colors.next()

    new_data = []
    for category in categories:
      new_data.append(data.get(category, 0.0))
    if last_data:
      bars = ax.bar(ind, new_data, width, color=color, bottom=last_data)
    else:
      bars = ax.bar(ind, new_data, width, color=color)
    
    last_data = new_data
    
    all_bars.append(bars[0])
    all_legend.append(name)

  ax.set_xticks(ind)
  ax.set_xticklabels(categories, rotation="vertical")

  prop = matplotlib.font_manager.FontProperties(size=8)
  ax.legend(all_bars, all_legend, loc=kwargs.get("legend_loc", "right"))

  if "title" in kwargs:   ax.set_title(kwargs["title"])
  if "x_label" in kwargs: ax.set_xlabel(kwargs["x_label"])
  if "y_label" in kwargs: ax.set_ylabel(kwargs["y_label"])
  if "y_lim" in kwargs:   ax.set_ylim(kwargs["y_lim"])

  return fig
  
def timeseries(series, **kwargs):
  """
      {
        "series1" : { 
          "x": [1,2,3...],
          "y": [4,5,6...],
          "style": "r--"
        },
        "series2" : {
          "x": [1,2,3...],
          "y": [10,11,12...]
        },
        ...
      }
  """
  fig = P.figure(figsize=(10,7))
  ax = fig.add_subplot(111)
  
  names = series.keys()
  names.sort()
  
  for name in names:
    x = series[name]["x"]
    y = series[name]["y"]
    style = series[name].get("style", "-")
    ax.plot(x, y, style)
    # ax.semilogy(x, y, style)

  prop = matplotlib.font_manager.FontProperties(size=8)
  ax.legend(names, prop=prop, loc=kwargs.get("legend_loc", "right"))
  
  if "title" in kwargs:   ax.set_title(kwargs["title"])
  if "x_label" in kwargs: ax.set_xlabel(kwargs["x_label"])
  if "y_label" in kwargs: ax.set_ylabel(kwargs["y_label"])
  if "y_lim" in kwargs:   ax.set_ylim(kwargs["y_lim"])
  
  return fig

def save_current(filename, dpi=200):
  if "/" in filename:
    pass
  else:
    directory = "/tmp/reports/"
    if not os.path.exists(directory):
      os.makedirs(directory)
    filename = "%s%s" % (directory, filename)  
  P.savefig(filename, dpi=dpi)
  return filename
  
if __name__ == '__main__':
  series = {
    "May" : {
      "Ford": 1,
      "Chevrolet": 2
    },
    "June" : {
      "Ford": 5,
      "Mazda": 7
    }
  }
  
  categories = ["Ford", "Chevrolet", "Mazda"]
  fig = clustered_columns(series, categories) 
  fig.show()
  raw_input()
