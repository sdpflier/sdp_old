#!/usr/bin/python
import sys
import os
import re
import datetime
import pymysql
import sshtunnel
import logging
import pdb

sys.path.insert(0,os.environ['SDP_ROOT'])


from utils.OpenConn import DbConnection as Conn

# Simple routine to run a query on a database and print the results:
def run( carsConn ) :
    carsCur = carsConn.cursor()
    carsCur.execute("show tables");
    for table in carsCur.fetchall():
        print(table)

if __name__ == "__main__":
    with Conn('sdp1') as c:
        carsdb = c.open_db('cars')   
        run(carsdb)
        carsdb.commit()
