#!/usr/bin/python
import sys
import os
import re
import pymysql
import sshtunnel
import logging
import pdb
sys.path.insert(0,os.environ['SDP_ROOT'])


from utils.OpenConn import DbConnection as Conn

_run_sql = \
"update \
    cars.models m \
        LEFT JOIN \
    (SELECT DISTINCT \
        make_name, model__name \
    FROM \
        clean_cars) cc ON m.name = cc.model__name \
 set m.active = 0 \
WHERE \
 m.end_year = 'present' \
 and cc.model__name is NULL;"

def sync_models( carsConn ):
    carsCur = carsConn.cursor()
    carsCur.execute(_run_sql)
    return carsCur.fetchall() 

if __name__ == "__main__":
    logging.basicConfig(filename='sync_models.log', filemode='w', level=logging.INFO)
    with Conn('localhost') as c:
        carsdb = c.open_db('cars')   
        sync_models(carsdb)
        results = carsdb.commit()
        logging.info(results)
    
