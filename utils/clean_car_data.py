#!/usr/bin/python
import sys
import os
import re
import pymysql
import sshtunnel
import logging
from decimal import Decimal
import pdb
sys.path.insert(0,os.environ['SDP_ROOT'])


from utils.OpenConn import DbConnection as Conn

_drop_clean_cars_table = \
  "drop table if exists clean_cars;"

_create_clean_cars_table = \
   "CREATE TABLE `clean_cars` (\
  `id` int(11) NOT NULL AUTO_INCREMENT,\
  `car_id` int(11) DEFAULT NULL,\
  `stock_dealer_code` varchar(60) DEFAULT NULL,\
  `make_name` varchar(45) DEFAULT NULL,\
  `model` varchar(100) DEFAULT NULL,\
  `model__name` varchar(60) DEFAULT NULL,\
  `stock_id` int(11) DEFAULT NULL,\
  `body_type` varchar(45) DEFAULT NULL,\
  `car_type` varchar(45) DEFAULT NULL,\
  `power_in_kw` varchar(45) DEFAULT NULL,\
  `model_start` varchar(45) DEFAULT NULL,\
  `model_series` varchar(45) DEFAULT NULL,\
  `manufacturer_internal_code` varchar(45) DEFAULT NULL,\
  `fuel_type` varchar(100) DEFAULT NULL,\
  `exterior_color` varchar(100) DEFAULT NULL,\
  `interior_color` varchar(100) DEFAULT NULL,\
  `year` varchar(4) DEFAULT NULL,\
  `base_price_cents` int(11) DEFAULT NULL,\
  `others_price` varchar(12) DEFAULT NULL,\
  `price_percent_diff` varchar(5) DEFAULT NULL,\
  `deal` varchar(20) DEFAULT NULL,\
  `others` blob,\
  `cleaned_at` datetime DEFAULT NULL,\
  `created_at` datetime not null DEFAULT CURRENT_TIMESTAMP,\
  `updated_at` datetime not null DEFAULT CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,\
  `deleted_at` datetime DEFAULT NULL,\
  PRIMARY KEY (`id`)\
) ENGINE=InnoDB DEFAULT CHARSET=utf8"

_price_buckets = \
   "SELECT\
   c.car_id,\
   d.make,\
   d.name,\
   ROUND(@othersprice := d.price * 1000) AS 'others price',\
   c.make_name,\
   c.model__name,\
   ROUND(@ourprice := c.base_price_cents / 100) AS 'our price',\
   round(@percentdiff := (@ourprice - @othersprice) * 100 / @othersprice) as '% diff',\
   case\
       when @percentdiff between -5 and 10 then 'fair'\
       when @percentdiff between -15 and -5 then 'good'\
       when @percentdiff < -10 then 'great'\
       when @percentdiff between 11 and 20 then 'high_price'\
       when @percentdiff > 20 then 'over_priced'\
    end as 'deal'\
   FROM\
     optimization.dk_models d\
   JOIN\
     cars.clean_cars c ON c.make_name = d.make\
     AND c.model__name = d.name;"

_valid_fields_cars = ['car_id','make_name','model','stock_id',\
                     'power_in_kw','model_start',\
                     'model_series','manufacturer_internal_code',\
                     'fuel_type','msrp',\
                     'stock_dealer_code','others','exterior_color',\
                     'interior_color','yead_version','year']

_valid_fields_cars_from_feed = ['car_id','brand','model','stock_id',\
                     'power_in_kw','model_start',\
                     'model_series','manufacturer_internal_code',\
                     'fuel_type','msrp',\
                     'stock_dealer_code','others','exterior_color',\
                     'interior_color','yead_version']

_exclude_fields = ['msrp','yead_version']

_valid_fields_clean_cars = [ x for x in _valid_fields_cars 
                             if x not in _exclude_fields ]
_valid_fields_clean_cars.extend(['model__name','base_price_cents'])

_select_fields_from_car =\
    "select " + ", ".join(_valid_fields_cars_from_feed) + \
    " from car where stock_dealer_code is not null" 

_select_from_models = "select name, title from models where make_name = '{}'"

_insert_to_clean_cars =\
    "insert into clean_cars (" + ", ".join(_valid_fields_clean_cars) + ") " + \
    "values ({});"

model_name_dict = { 
                    'a5-sportback':['A5 SB'], \
                    'a5-coupe':['A5 Coupi'], \
                    'a7-sportback':['A7 Spb.'],\
                    'rs3-sportback':['RS 3 Sport'],\
                    'tt-coupe':['TT Coupi'], \
                    'ceed-sw':['CEEDSW'], \
                    'passat-variant':['PAS.VAR']
                  }
_db_user = 'root'
_db_password=''
_db_kon_name = 'konfigurator_staging'
_db_cars_name = 'cars'

def get_year(yead):
    if(re.search('-',yead)):
      return '20' + yead.split('-')[1]
    else:
      return None
    
    
def format_base_price(price):
    out = None
    if isinstance(price, Decimal):
        price = str(price)
    if price != None:
        if '.' in price:
            [front, back] = price.split('.')
            if len(front) < 4:
                out = front + back.ljust(3,'0') + '00'
            else:
                out = front + back.ljust(2,'0')
        else:
            if len(price) < 6 and len(price) > 3:
                out = price + '00'
            else:
                out = None 
    return out

def format_make(make_name):
    return make_name.lower()

def standardize_model():
    return None

def get_models(make, cur):
    cur.execute(_select_from_models.format(make))
    models = cur.fetchall()
    return {name: title for (name, title) in models}
    
def clean_model_bit(bit):
    return bit.lower().replace('!','')
    #sportback here

def search(myDict, search1):
    for key, values in myDict.items():
        for value in values:
            if value in search1:
                return key

def decide_site_model(modelHash, carh, carsCur):
    #pull make, type, model_series, model
    make = carh['make_name'].lower()
    if make not in modelHash:
        modelHash[make] = get_models(make, carsCur)
    parsed_model = carh['model'].split(' ')
    # remove unwanted bits from model split
    parsed_model = [ clean_model_bit(x) for x in parsed_model
                     if ((make not in x.lower()) 
                    and ('nuova' not in x.lower())
                    and ('' !=  x) 
                    and ('.' not in x))]
    # attempt to create a key starting with the longest to the shortest
    for i in [3,2,1]:
        can_key = '-'.join(parsed_model[:i])
        if can_key in modelHash[make].keys():
            carh['model__name'] = can_key
            break
    # attempt to use the second bit as a standalone key
    if 'model__name' not in carh.keys():
        if parsed_model[1] in modelHash[make].keys():
            carh['model__name'] = parsed_model[1]
    # attempt to use the third bit as a standalone key
    if 'model__name' not in carh.keys():
        if parsed_model[2] in modelHash[make].keys():
            carh['model__name'] = parsed_model[2]
    # have a dict of custom model names that dont fit 
    if 'model__name' not in carh.keys():
        carh['model__name']= search(model_name_dict, carh['model'])

    return [modelHash, carh]

def xstr(s):
    if s is None:
      return 'NULL'
    else:
      s = re.sub(r'[^\x00-\x7F]+',' ', s).encode('utf-8')
      return re.escape(str(s))

def ensureStr(s):
    if s is None:
        return ''
    else:
        return str(s)

def insert_to_clean_cars(carh, success, total, carsCur):
    carh_values = [xstr(carh[key]) for key in _valid_fields_clean_cars]
    sql = _insert_to_clean_cars.format("'" + "', '".join(carh_values) + "'")
    sql = sql.replace("'NULL'",'NULL')
    print sql
    carsCur.execute(sql)

# Simple routine to run a query on a database and print the results:
def clean_cars( carsConn ) :
    logging.basicConfig(filename='clean_cars.log', filemode='w', level=logging.DEBUG)
    carsCur = carsConn.cursor()
    modelHash = {}
    total = 0
    success = 0

    carsCur.execute(_drop_clean_cars_table)
    carsCur.execute(_create_clean_cars_table)
    carsCur.execute(_select_fields_from_car)

    for car in carsCur.fetchall():
        total += 1
        carh = {key: value for (key, value) in zip(_valid_fields_cars, car)}
        carh['make_name'] = format_make(carh['make_name']) 
        carh['base_price_cents'] = format_base_price(carh['msrp'])
        carh['year'] = get_year(carh['yead_version'])
        [modelHash, carh] = decide_site_model(modelHash, carh, carsCur)
        if 'model__name' in carh:
            success += 1
        else:
            carh['model__name'] = None
        
        if carh['others'] is not None:
          carh['others'] = clean_cars_field( carh['others'] )

        insert_to_clean_cars(carh, success, total, carsCur)
        logging.debug(ensureStr(carh['make_name']) + ' ' + ensureStr(carh['model']) +  ' ' + ensureStr(str(carh['model__name'])) + ' ' + ensureStr(carh['msrp']) + ' ' + ensureStr(str(success/(total * 1.0))) )

def clean_cars_field( others ):
    others = re.sub(r'[ ]+',' ',others)
    others = re.sub(r',([^\s])',r', \1',others)
    return others

def add_body_type( carsConn):
    logging.basicConfig(filename='clean_cars_body_type.log', filemode='w', level=logging.DEBUG)
    carsCur = carsConn.cursor()
    
    carsCur.execute("select distinct name, make_name, body_type from models")
    for model in carsCur.fetchall():
        carsCur.execute('update clean_cars set body_type = "{0}" where model__name = "{1}" and make_name = "{2}";'.format(model[2], model[0], model[1]))
    
def add_price_bucket( carsConn):
    logging.basicConfig(filename='clean_cars_price_bucket.log', filemode='w', level=logging.DEBUG)
    carsCur = carsConn.cursor()
    
    carsCur.execute(_price_buckets)
    for model in carsCur.fetchall():
        carsCur.execute('update clean_cars cc set others_price = "{0}", price_percent_diff = "{1}", deal = "{2}" where car_id = {3};'.format(model[3], model[7], model[8], model[0]))

if __name__ == "__main__":
    with Conn('localhost') as c:
        carsdb = c.open_db('cars')   
        clean_cars(carsdb)
        add_body_type(carsdb)
        add_price_bucket(carsdb)
        carsdb.commit()
