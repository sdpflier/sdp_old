#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Nov  7 10:20:10 2018

@author: tassinaro 

Example ~/.ssh/config

Host sdp1
  HostName 18.185.236.69
  User ec2-user
  IdentityFile ~/.ssh/id_rsa

"""
import os
import pymysql
import socket
import paramiko
import pdb
from sshtunnel import SSHTunnelForwarder
from contextlib import closing

class DbConnection:
    def __enter__ (self):
        self.start_ssh_with_check(self.host)
        return self
        
    def __exit__ (self, type, value, tb):
        try:
            for d in self.db:
                d.close()
        except:
            if self.server != None:
                self.server.stop()
        finally:
            if self.server != None:
                self.server.stop()
        print('closing connections: OK')
        

    def __init__(self, host, config_file= '~/.ssh/config'):
        self._remote_bind_address = '127.0.0.1'
        self._remote_mysql_port = 3306
        self._local_bind_address = '127.0.0.1'
        self._local_mysql_port = 3006
        self.server = None
        self.host = host
        self.ssh_config_file = config_file
        self.db = []
        # this can be in an external config file
        self.dbs= {
            'cars': {
                'db_user': 'root',
                'db_password':'',
                'db_name':'cars'
                },
            'kons': {
                'db_user': 'root',
                'db_password':'',
                'db_name':'konfigurator_staging'
                },
            'konp': {
                'db_user': 'root',
                'db_password':'',
                'db_name':'konfigurator_production'
                },
            'opt': {
                'db_user': 'root',
                'db_password':'',
                'db_name':'optimization'
                },
            'carsl': {
                'db_user': 'root',
                'db_password':'',
                'db_name':'cars'
                },
            'kondl': {
                'db_user': 'root',
                'db_password':'',
                'db_name':'konfigurator_development'
                }
            }

    def check_socket_open(self, host, port):
        with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as sock:
            if sock.connect_ex((host, port)) == 0:
                return True
            else:
                return False

    def start_ssh(self, host):
        self.server = SSHTunnelForwarder(host,
            ssh_config_file=self.ssh_config_file,
            remote_bind_address=(self._remote_bind_address, self._remote_mysql_port),
            local_bind_address=(self._local_bind_address, self._local_mysql_port))
        self.server.start()
        print ('opening server: OK')


    def check_if_host_is_local(self):
        ssh_config_file = os.path.expanduser(self.ssh_config_file)
        if os.path.exists(ssh_config_file):
            conf = paramiko.SSHConfig()
            with open(ssh_config_file) as f:
                conf.parse(f)
            host_config = conf.lookup(self.host)
            return host_config['hostname'] in ['localhost', '127.0.0.1']


    def start_ssh_with_check(self, host):
        if not self.check_if_host_is_local():
            if not self.check_socket_open(self._local_bind_address, 
                                          self._local_mysql_port):
                self.start_ssh(host)
        else:
            self._local_mysql_port = self._remote_mysql_port
        
    def open_db(self, db_name):
        db = pymysql.connect(user=self.dbs[db_name]['db_user'],
                             passwd=self.dbs[db_name]['db_password'],
                             host=self._local_bind_address,
                             db=self.dbs[db_name]['db_name'],
                             port=self._local_mysql_port)
        self.db.append(db)
        print ('opening database: OK')
        return db
        

if __name__ == "__main__":
    with DbConnection('sdp1') as dbc:
        dbc.open_db('cars')


