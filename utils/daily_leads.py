#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 21 08:43:44 2018

@author: jovanna
"""
from OpenConn import DbConnection as Conn
from mail import Email

query = """
SELECT date,source,MAX(CASE WHEN name = 'arrivals' THEN count ELSE 0 END) AS arrivals,
MAX(CASE WHEN name='index' THEN count ELSE 0  END) AS 'index',MAX(CASE WHEN name = 
'poll_success' THEN count ELSE 0 END) AS poll_success,MAX(CASE WHEN name = 'dealer-select' 
THEN count ELSE 0 END) AS dealer_select FROM (SELECT DATE(f.created_at) date, 
IF(a.arrival_url LIKE '%gclid%','Google', IF(a.arrival_url LIKE '%msclkid%','Bing',
'Organic')) source,f.name,COUNT(*) count FROM forms f JOIN arrivals a ON 
f.arrival_id = a.id  GROUP BY 1 , 2 , 3 UNION SELECT DATE(a.created_at) date,     
IF(a.arrival_url LIKE '%gclid%','Google', IF(a.arrival_url LIKE '%msclkid%','Bing',
'Organic')) source, 'arrivals',COUNT(*) count FROM arrivals a WHERE a.user_agent NOT 
LIKE '%Bot%' AND a.arrival_url LIKE '%?m%' GROUP BY 1,2,3) a GROUP BY 1,2 ORDER BY 1
desc limit 18;
"""

rows = []

style_header = '''<style>
table { border-collapse: collapse;}
td, th { border: 1px solid #000000; vertical-align: baseline; padding: 3}
</style>
'''

def construct_message_text(rows,columns,header_text='',styled=False):
    text = "%s<br><table><tr>" % header_text
    for key in columns:
        text += "<td><strong>%s</strong></td>" % key
    text += "</tr>"
    for row in rows:
        text += "<tr>"
        for key in columns:
            text += "<td>%s</td>" % row[key]
        text += "</tr>"
    text += "</table>"

    if styled:
        text = style_header + text
        replaces = [
            ('<table><tr>','<table><tr bgcolor=yellow>'), # highlight first row
        ]
        for old, new in replaces:
            text = text.replace(old,new)

    return text


def pull_report(db):
    cursor = db.cursor()
    cursor.execute(query)
    for row in cursor.fetchall():
        results = {}
        date, source, arrivals, index, poll_success, dealer_select = row
        #print row
        results["date"] = date
        results["source"] = source
        results["arrivals"] = arrivals
        results["car_search"] = index
        results["coverage_found"] = poll_success
        results["leads"] = dealer_select
        #print results
        rows.append(results)


column_headers = ["date", "source", "arrivals", "car_search", "coverage_found", "leads"]

if __name__ == "__main__":
    with Conn('sdp1') as c:
        conn = c.open_db('konp')   
        pull_report(conn)
        conn.commit()
    message_text = construct_message_text(rows,column_headers,header_text="")
    #recipients=("reporting@groupsdp.com","Verica.Lukic@evolvea.rs")
    #print message_text
    m = Email()
    mFrom = "SDP Report <notifications@groupsdp.com>"
    m.setFrom(mFrom)
    m.setSubject("[Report] Daily user activity")
    m.addRecipient("reporting@groupsdp.com")
    #for recipient in recipients:
       # m.addRecipient(recipient)
    m.setHtmlBody(message_text)
    m.send()
    print('Task completed')   

