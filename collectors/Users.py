#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 13 13:29:38 2018

@author: jovanna
"""
from utils.OpenConn import DbConnection as Conn

query = """
SELECT 
    REPLACE(JSON_EXTRACT(data, '$."first-name"'),
        '"',
        '') AS name,
    REPLACE(JSON_EXTRACT(data, '$."last-name"'),
        '"',
        '') AS lastname,
    REPLACE(JSON_EXTRACT(data, '$.email'),
        '"',
        '') AS email,
    JSON_EXTRACT(data, '$."phone_number"') AS phone_number,
    a.created_at
FROM
    konfigurator_production.arrivals a
        JOIN
    konfigurator_production.forms f ON a.id = f.arrival_id
        AND user_agent NOT LIKE '%Bot%'
        AND data NOT LIKE '%test%'
        AND data NOT LIKE '%Test%'
        AND JSON_EXTRACT(data, '$.email') IS NOT NULL;
"""

def process(db):
    rows_updated = 0
    cursor = db.cursor()
    cursor.execute(query)    
    for row in cursor.fetchall():
        user = {}
        name,lastname,email,phone_number,created_at = row
        user["name"] = name
        user["lastname"] = lastname
        user["email"] = email
        user["phone_number"] = phone_number
        user["created_at"] = created_at.strftime('%Y-%m-%d %H:%M:%S')
        #get rid of empty fields
        user = dict((k, v) for k, v in user.iteritems() if v is not None)
        count = update_db(user,conn)
        rows_updated = count + rows_updated
    print 'Database updated: %d insertions' % rows_updated
        
def update_db(data,db):
    cursor = db.cursor()
    columns = ', '.join(data.keys())
    values = tuple(data.values())
    qry = "insert ignore into optimization.users (%s) values %s" % (columns,values)
    cursor.execute(qry)    
    conn.commit()
    return cursor.rowcount

if __name__ == "__main__":
    with Conn('sdp1') as c:
        conn = c.open_db('konp')   
        process(conn)
        
                         
        
        