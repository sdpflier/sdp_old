#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 13 13:29:03 2018

@author: jovanna
"""

import MySQLdb
import time
from autotegrity.optimization.odp.common import blogging
from autotegrity import db
from autotegrity.utils.utils import get_whitelabel_publishers

logger = blogging.get_logger("com.autotegrity.optimization.odp.collectors.Arrivals")

SQL_ARRIVALS = """
/* Arrivals collector */
SELECT 
a.id,arrival_url,user_agent,ip,ap.id,name,value,ap.created_at
FROM konfigurator_production.arrivals a
JOIN konfigurator_production.arrival_properties ap ON a.id = ap.arrival_id
#AND a.created_at > CURDATE()
"""
__IGNORES_RANGE_BY__ = True

def collect(start_date, end_date, range_by='created_at', verbose=False):
  conn = db.connection("konp")
  cursor = conn.cursor(MySQLdb.cursors.DictCursor)

  conn2 = db.connection("replication_sites_cloud", "scratch")
  cursor2 = conn2.cursor(MySQLdb.cursors.DictCursor)

  output = []
  start = time.time()

  for site_db, site_name, constraint in CLIENTS:
    logger.info("Collecting arrivals from site: %s, [%s] for date range %s --> %s." % (site_name, site_db, start_date, end_date))

    SQL = SQL_ARRIVALS % {
      "SITE_DB":site_db, "SITE_NAME":site_name, "CONSTRAINT":"",
      "START_DATE":start_date, "END_DATE":end_date
      }

    db.execute(cursor, SQL, query_name=__file__)
    logger.info("Got %s rows." % cursor.rowcount)
    output += cursor.fetchall()

    # New
    if constraint is None:
      constraint = "and (request_host is null or request_host like '%%%%%s%%%%')" % site_name

    SQL = SQL_ARRIVALS % {
      "SITE_DB":site_db, "SITE_NAME":site_name, "CONSTRAINT":constraint,
      "START_DATE":start_date, "END_DATE":end_date
      }

    db.execute(cursor2, SQL, query_name=__file__)
    logger.info("Got %s rows." % cursor2.rowcount)

    output += cursor2.fetchall()

  conn.close()
  conn2.close()

  conn = db.connection("replication_sl", "scratch")
  cursor = conn.cursor(MySQLdb.cursors.DictCursor)

  for partner, publisher_id in (
    ("getastart.com", 90),
    ("usnews.com", 91),
    ("lemonfree.com", 92),
    ):
    logger.info("Collecting arrivals from partner: %s, [%s] for date range %s --> %s." % (partner, publisher_id, start_date, end_date))

    SQL = SQL_PARTNER % {
      "PARTNER_NAME": partner, "PUBLISHER_ID": publisher_id,
      "START_DATE":start_date, "END_DATE":end_date
      }

    db.execute(cursor, SQL, query_name=__file__)
    logger.info("Got %s rows." % cursor.rowcount)

    output += cursor.fetchall()

  conn.close()

  conn = db.connection("replication_tie", "slave_only")
  cursor = conn.cursor(MySQLdb.cursors.DictCursor)

  logger.info("Collecting arrivals from: tradeinexpert for date range %s --> %s." % (start_date, end_date))

  SQL = SQL_ARRIVALS_TIE % {
    "SITE_NAME":"tradeinexpert.com", "CONSTRAINT":"",
    "START_DATE":start_date, "END_DATE":end_date
  }

  db.execute(cursor, SQL, query_name=__file__)
  logger.info("Got %s rows." % cursor.rowcount)

  output += cursor.fetchall()

  conn.close()
  end = time.time()

  return {"data":output, "query_rate":len(output)/max((end-start), 1E-10), "total_time":(end-start)}

def process_arrivals(arrivals):
  import copy

  from autotegrity.optimization.odp.common import schema, sem, arrival_ids
  from autotegrity.optimization.intent import label_intent
  from autotegrity import db, config

  import httpagentparser
  import pygeoip
  from mobile.sniffer.detect import detect_mobile_browser
  from urlparse import urlparse, parse_qs

  import datetime

  start = time.time()

  data_dir = config.get('autotegrity', 'data_dir')
  gic = pygeoip.GeoIP("%s/autotegrity/optimization/odp/facts/GeoLiteCity.dat" % data_dir)
  gio = pygeoip.GeoIP("%s/autotegrity/optimization/odp/facts/GeoIPISP.dat" % data_dir)

  output = []

  s = schema.Schema()

  conn = db.connection("replication_sites_cloud", "pixel")
  cursor = conn.cursor()

  for arrival in arrivals:
    columns = copy.copy(arrival)

    source = columns["source"]
    arrival_id = columns["arrival_id"]
    global_arrival_id = "%s:%s" % (source, arrival_id)

    PROCESS_GUIDS = True
    if PROCESS_GUIDS:
      # Get global_arrival_id
      postfix_id = arrival_ids.global_arrival_id_to_postfix_id(global_arrival_id)

      if postfix_id is None:
        global_user_id = global_arrival_id
      else:
        try:
          db.execute(cursor, """
  /* process_arrivals */
  select
   arrival_id as earliest_arrival_id
  from pixel.guids
  where guid = (
  select guid
   from pixel.guids
  where arrival_id = %s
  order by created_at asc
  limit 1)
  order by created_at asc
  limit 1""", (postfix_id), __file__)
        except Exception, e:
          print postfix_id, global_arrival_id
          raise e
        earliest_postfix_id = cursor.fetchone()
        if earliest_postfix_id:
          global_user_id = arrival_ids.postfix_id_to_global_arrival_id(earliest_postfix_id[0])
        else:
          global_user_id = global_arrival_id
    else:
      global_user_id = global_arrival_id

    # camick 2012-08-09 -- Handle newcars.motovy.com
    if arrival["source"] == "newcars.motovy.com":
      global_arrival_id += ":%s" % arrival["site_db"]

    columns["global_arrival_id"] = global_arrival_id
    columns["global_user_id"] = global_user_id

    # Traffic partner name = MSN for all Yahoo after 2010-10-27
    if columns["traffic_partner_name"] == "Yahoo" and columns["created_at"] >= datetime.datetime(year=2010, month=10, day=27):
      columns["traffic_partner_name"] = "MSN"

    # IP parts
    ip_class_a = None
    ip_class_b = None
    ip_class_c = None

    ip = columns["ip"]
    if ip:
      ip_parts = ip.split(".")
      ip_class_a = ip_parts[0]
      ip_class_b = ".".join(ip_parts[0:2])
      ip_class_c = ".".join(ip_parts[0:3])

    columns["ip_class_a"] = ip_class_a
    columns["ip_class_b"] = ip_class_b
    columns["ip_class_c"] = ip_class_c

    # User Agent parsing
    user_agent = columns["user_agent"]

    os_name = None
    browser_name = None
    browser_version = None

    if user_agent:
      x = httpagentparser.detect(user_agent)
      if "os" in x:
        if "name" in x["os"]:
          os_name = x["os"]["name"]

      if "browser" in x:
        if "name" in x["browser"]:
          browser_name = x["browser"]["name"]

        if "version" in x["browser"]:
          browser_version = x["browser"]["version"]

    columns["os_name"] = os_name
    columns["browser_name"] = browser_name
    columns["browser_version"] = browser_version

    # Query string parsing
    referer = columns["referer"]
    search_query = sem.parse_search_query(referer)
    columns["search_query"] = search_query

    # Refering domain parsing
    referring_domain_full = None
    referring_domain_top = None

    if referer is not None:
      pr = urlparse(referer)
      referring_domain_full = pr.netloc
      query = pr.query
      if referring_domain_full == "":
        referring_domain_full = None
      if referring_domain_full:
        referring_domain_top = ".".join(referring_domain_full.split(".")[-2:])

    # AdSense domain handling
    if (referring_domain_top is not None
    and referring_domain_top.lower() in ("doubleclick.net", "google.com", "googlesyndication.com")
    and query != ""
    and (arrival["traffic_partner_name"] == "AdSense" or arrival["traffic_type_name"] == "Display")):
      # Parse AdSense referer
      parsed_qs = parse_qs(query)

      referer = parsed_qs.get("url", parsed_qs.get("ref", [None]))[0]

      #print referer
      #if referer is None: print parsed_qs

      referring_domain_full = None
      referring_domain_top = None

      if referer is not None:
        pr = urlparse(referer)
        referring_domain_full = pr.netloc
        if referring_domain_full == "":
          referring_domain_full = None
        if referring_domain_full:
          referring_domain_top = ".".join(referring_domain_full.split(".")[-2:])

    columns["referring_domain_full"] = referring_domain_full
    columns["referring_domain_top"] = referring_domain_top


    # IP Geo parsing
    ip_geo_zip = None
    ip_geo_city = None
    ip_geo_msa = None
    ip_geo_state = None
    ip_geo_country = None
    isp = None

    if ip:
      geo = gic.record_by_addr(ip)
      if geo:
        ip_geo_zip = geo.get("postal_code")
        if ip_geo_zip:
          ip_geo_zip = ip_geo_zip[0:5]
        ip_geo_city = geo.get("city")
        # TODO
        ip_geo_msa = None
        ip_geo_state = geo.get("region_name")
        ip_geo_country = geo.get("country_name")

      isp = gio.org_by_addr(ip)
      if isp == '': isp = None

    columns["ip_geo_zip"] = ip_geo_zip
    columns["ip_geo_city"] = ip_geo_city
    columns["ip_geo_msa"] = ip_geo_msa
    columns["ip_geo_state"] = ip_geo_state
    columns["ip_geo_country"] = ip_geo_country
    columns["isp"] = isp

    # Intent parsing
    intent_dict = label_intent.label_intent(columns["referer"], columns["keyword"])

    search_intent = intent_dict["search_intent"]
    search_intent_quality = None
    columns["search_intent"] = search_intent
    columns["search_intent_quality"] = search_intent_quality

    is_mobile = None
    if user_agent:
      is_mobile = detect_mobile_browser(user_agent)
    columns["is_mobile"] = is_mobile

    # camick -- Audience names in keywords for GoogleDisplay Audiences
    if columns['traffic_source_id'] in (474, 261):
      columns['keyword'] = columns['audience']

    # MSN hack: Look for match_type
    if columns["traffic_partner_name"] == "MSN" and not columns["is_bot"]:
      request_uri = columns["request_uri"]

      MAP = {
        4 : {
          "exact" : {
            "traffic_source_id" : 6,
            "traffic_source_name" : "MSNExact",
          },
          "phrase" : {
            "traffic_source_id" : 7,
            "traffic_source_name" : "MSNPhrase",
          },
          "broad" : {
            "traffic_source_id" : 8,
            "traffic_source_name" : "MSNBroad",
          },
        },
        2 : {
          "exact" : {
            "traffic_source_id" : 6,
            "traffic_source_name" : "MSNExact",
          },
          "phrase" : {
            "traffic_source_id" : 7,
            "traffic_source_name" : "MSNPhrase",
          },
          "broad" : {
            "traffic_source_id" : 8,
            "traffic_source_name" : "MSNBroad",
          },
        },
	6 : {
          "exact" : {
            "traffic_source_id" : 6,
            "traffic_source_name" : "MSNExact",
          },
          "phrase" : {
            "traffic_source_id" : 7,
            "traffic_source_name" : "MSNPhrase",
          },
          "broad" : {
            "traffic_source_id" : 8,
            "traffic_source_name" : "MSNBroad",
          },
	},
        7 : {
          "exact" : {
            "traffic_source_id" : 6,
            "traffic_source_name" : "MSNExact",
          },
          "phrase" : {
            "traffic_source_id" : 7,
            "traffic_source_name" : "MSNPhrase",
          },
          "broad" : {
            "traffic_source_id" : 8,
            "traffic_source_name" : "MSNBroad",
          },
        },
	9 : {
          "exact" : {
            "traffic_source_id" : 6,
            "traffic_source_name" : "MSNExact",
          },
          "phrase" : {
            "traffic_source_id" : 7,
            "traffic_source_name" : "MSNPhrase",
          },
          "broad" : {
            "traffic_source_id" : 8,
            "traffic_source_name" : "MSNBroad",
          },
	},
        183 : {
          "exact" : {
            "traffic_source_id" : 184,
            "traffic_source_name" : "MSN Strategy 1 Exact",
          },
          "phrase" : {
            "traffic_source_id" : 185,
            "traffic_source_name" : "MSN Strategy 1 Phrase",
          },
          "broad" : {
            "traffic_source_id" : 186,
            "traffic_source_name" : "MSN Strategy 1 Broad",
          },
        },
        110 : {
          "exact" : {
            "traffic_source_id" : 184,
            "traffic_source_name" : "MSN Strategy 1 Exact",
          },
          "phrase" : {
            "traffic_source_id" : 185,
            "traffic_source_name" : "MSN Strategy 1 Phrase",
          },
          "broad" : {
            "traffic_source_id" : 186,
            "traffic_source_name" : "MSN Strategy 1 Broad",
          },
        },
        220 : {
          "exact" : {
            "traffic_source_id" : 221,
            "traffic_source_name" : "MSN Strategy 2 Exact",
          },
          "phrase" : {
            "traffic_source_id" : 222,
            "traffic_source_name" : "MSN Strategy 2 Phrase",
          },
          "broad" : {
            "traffic_source_id" : 223,
            "traffic_source_name" : "MSN Strategy 2 Broad",
          },
        },
        225 : {
          "exact" : {
            "traffic_source_id" : 226,
            "traffic_source_name" : "MSN Strategy 3 Exact",
          },
          "phrase" : {
            "traffic_source_id" : 227,
            "traffic_source_name" : "MSN Strategy 3 Phrase",
          },
          "broad" : {
            "traffic_source_id" : 228,
            "traffic_source_name" : "MSN Strategy 3 Broad",
          },
        },
       610 : {
          "exact" : {
            "traffic_source_id" : 613,
            "traffic_source_name" : "MSN Single Matchtype Exact",
          },
          "phrase" : {
            "traffic_source_id" : 612,
            "traffic_source_name" : "MSN Single Matchtype Phrase",
          },
          "broad" : {
            "traffic_source_id" : 611,
            "traffic_source_name" : "MSN Single Matchtype Broad",
          },
       },
      }

      if "m=e" in request_uri:
        match_type = "exact"
      elif "m=p" in request_uri:
        match_type = "phrase"
      elif "m=b" in request_uri:
        match_type = "broad"
      else:
        print "*** NO MATCH TYPE", request_uri
        match_type = None

      if match_type and columns["traffic_source_id"] in MAP:
        mapping = MAP[columns["traffic_source_id"]][match_type]
        columns["traffic_source_id"] = mapping["traffic_source_id"]
        columns["traffic_source_name"] = mapping["traffic_source_name"]

    ordered = s.get_ordered_columns(columns, "Arrivals")
    output.append(ordered)

  conn.close()

  end = time.time()
  return {"data":output, "rate":len(arrivals)/max((end-start), 1E-10), "total_time":(end-start)}

def process_arrival_query_parameters(arrivals):
  from autotegrity.optimization.odp.common import schema
  from urlparse import urlparse, parse_qs

  start = time.time()
  output = []

  s = schema.Schema()

  for arrival in arrivals:
    columns = {}

    source = arrival["source"]
    arrival_id = arrival["arrival_id"]
    global_arrival_id = "%s:%s" % (source, arrival_id)

    # camick 2012-08-09 -- Handle newcars.motovy.com
    if arrival["source"] == "newcars.motovy.com":
      global_arrival_id += ":%s" % arrival["site_db"]

    columns["global_arrival_id"] = global_arrival_id

    request_uri = arrival["request_uri"]

    if request_uri is not None:
      pr = urlparse(request_uri)
      qs = pr.query

      if qs != "":
        qp = parse_qs(qs)
        for name, values in qp.iteritems():
          for value in values:
            columns["name"] = name
            columns["value"] = value

            ordered = s.get_ordered_columns(columns, "ArrivalQueryParameters")
            output.append(ordered)

  end = time.time()
  return {"data":output, "rate":len(arrivals)/max((end-start), 1E-10), "total_time":(end-start)}

def process_search_tokens(arrivals):
  from autotegrity.optimization.odp.common import schema
  from autotegrity.optimization.odp.common import sem
  from urlparse import urlparse, parse_qs

  start = time.time()
  output = []

  s = schema.Schema()

  for arrival in arrivals:
    columns = {}

    source = arrival["source"]
    arrival_id = arrival["arrival_id"]
    global_arrival_id = "%s:%s" % (source, arrival_id)

    # camick 2012-08-09 -- Handle newcars.motovy.com
    if arrival["source"] == "newcars.motovy.com":
      global_arrival_id += ":%s" % arrival["site_db"]

    columns["global_arrival_id"] = global_arrival_id

    referer = arrival["referer"]

    search_query = sem.parse_search_query(referer)

    if search_query:
      tokens = search_query.split(" ")
      for token in tokens:
        if token.strip() == "":
          continue
        columns["token"] = token

        ordered = s.get_ordered_columns(columns, "SearchTokens")
        output.append(ordered)

  end = time.time()
  return {"data":output, "rate":len(arrivals)/max((end-start), 1E-10), "total_time":(end-start)}

def process_referer_parameters(arrivals):
  from autotegrity.optimization.odp.common import schema
  from urlparse import urlparse, parse_qs

  start = time.time()
  output = []

  s = schema.Schema()

  for arrival in arrivals:
    columns = {}

    source = arrival["source"]
    arrival_id = arrival["arrival_id"]
    global_arrival_id = "%s:%s" % (source, arrival_id)

    # camick 2012-08-09 -- Handle newcars.motovy.com
    if arrival["source"] == "newcars.motovy.com":
      global_arrival_id += ":%s" % arrival["site_db"]

    columns["global_arrival_id"] = global_arrival_id

    referer = arrival["referer"]

    if referer is not None:
      pr = urlparse(referer)
      query = pr.query
      if query != "":
        parsed_qs = parse_qs(query)
        for (name, values) in parsed_qs.iteritems():
          columns["name"] = name
          for value in values:
            columns["value"] = value

            ordered = s.get_ordered_columns(columns, "RefererParameters")
            output.append(ordered)

  end = time.time()
  return {"data":output, "rate":len(arrivals)/max((end-start), 1E-10), "total_time":(end-start)}

def estimate_cost(start_date, end_date, range_by='created_at'):
  conn = db.connection("replication_sites", "scratch")
  cursor = conn.cursor(MySQLdb.cursors.DictCursor)

  conn2 = db.connection("replication_sites_cloud", "scratch")
  cursor2 = conn2.cursor(MySQLdb.cursors.DictCursor)

  total_query_cost = 0.0
  for site_db, site_name, constraint in (
    ("sas_production", "smartautosavings.com", None),
    ("apf_production", "auto-price-finder.com", None),
    ("fac_production", "123findacar.com", None),
    ("apf_production", "lemonfree.com", "and request_host like '%%lemonfree.com%%'"),
    ("apf_production", "smartautooffers.com", "and request_host like '%%smartautooffers.com%%'"),
    ("apf_production", "tradeinexpert.com", "and request_host like '%%tradeinexpert.com%%'")
    ):

    SQL = SQL_ARRIVALS % {
      "SITE_DB":site_db, "SITE_NAME":site_name, "CONSTRAINT":"",
      "START_DATE":start_date, "END_DATE":end_date
      }

    query_cost, scanned_rows = db.estimate_query_cost(conn, SQL)
    total_query_cost += query_cost

    if constraint is None:
      constraint = "and (request_host is null or request_host like '%%%%%s%%%%')" % site_name

    SQL = SQL_ARRIVALS % {
      "SITE_DB":site_db, "SITE_NAME":site_name, "CONSTRAINT":constraint,
      "START_DATE":start_date, "END_DATE":end_date
      }

    query_cost, scanned_rows = db.estimate_query_cost(conn2, SQL)
    total_query_cost += query_cost

  conn.close()
  conn2.close()
  conn = db.connection("replication_sl", "scratch")

  for partner, publisher_id in (
    ("getastart.com", 90),
    ("usnews.com", 91),
    ("lemonfree.com", 92),
    ):

    SQL = SQL_PARTNER % {
      "PARTNER_NAME": partner, "PUBLISHER_ID": publisher_id,
      "START_DATE":start_date, "END_DATE":end_date
      }

    query_cost, scanned_rows = db.estimate_query_cost(conn, SQL)
    total_query_cost += query_cost

  conn.close()

  return total_query_cost

# --------
INCREMENTAL_START_CHECKS = [
    ("source in ('auto-price-finder.com', 'smartautosavings.com', '123findacar.com', "
     "'smartautooffers.com', 'auto-price-saver.com', 'instantautoprices.com', "
     "'newcars.motovy.com', 'newcars.dealernet.com', 'whitelabels.com', 'tradeinexpert.com')"),
    "source = 'tradeinexpert.com'",
]

__MUTATORS__ = {
  "process_arrivals":{"func":process_arrivals, "schema_name":"Arrivals", "smart_load":True,
                     "lag_error_thresh":3600, "incremental_start_checks":INCREMENTAL_START_CHECKS},
  "process_arrival_query_parameters":{"func":process_arrival_query_parameters,
                                      "schema_name":"ArrivalQueryParameters", "smart_load":False,
                                      "lag_error_thresh":3600},
  "process_search_tokens":{"func":process_search_tokens, "schema_name":"SearchTokens",
                           "smart_load":False, "lag_error_thresh":3600},
}

if __name__ == "__main__":
  import datetime
  end_date = datetime.datetime.now() - datetime.timedelta(hours=2)
  start_date = end_date - datetime.timedelta(minutes=15)
  verbose = False

  arrivals = collect(start_date, end_date, verbose)

  import time
  start = time.time()
  process_arrivals(arrivals["data"])
  end = time.time()
  print "process_arrivals took %.2f seconds for %d arrivals" % (end-start, len(arrivals))
  #process_arrival_query_parameters(arrivals)
  #process_search_tokens(arrivals)
  #process_referer_parameters(arrivals)
