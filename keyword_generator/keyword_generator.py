#!/usr/bin/python
import pdb

from sdp.utils.OpenConn import DbConnection as Conn
from optparse import OptionParser

parser = OptionParser()
parser.add_option("-r", "--rebuild", action='store_true',
                  help="Rebuild database table from keywords.csv")
parser.add_option("-l", "--language", default='it',
                  help="Select the language that should be run. Default it. (en, it, both)")
parser.add_option("-f", "--file", 
                  help="Enter filename where keywords should be stored. Default is to screen")
parser.add_option("-m", "--matchtype", 
                  help="Set matchtype. '','modifier'")
(options, args) = parser.parse_args()

KEYS = ['kind_of_cars',
'style_of_cars',
'fuel_types',
'features',
'makes',
'make_models',
'verbs',
'adjectives',
'nouns']

#Active: en, it, both
LANGUAGE = options.language 

_drop_keyword_components = \
   "DROP TABLE IF EXISTS `keyword_components`;"

_keyword_components_table= \
   "CREATE TABLE IF NOT EXISTS `keyword_components` (\
  `id` int(11) NOT NULL AUTO_INCREMENT,\
  `group_name` varchar(40) DEFAULT NULL,\
  `type` varchar(20) DEFAULT NULL,\
  `language` varchar(5) DEFAULT NULL,\
  `component` varchar(40) DEFAULT NULL,\
  `created_at` datetime DEFAULT NULL,\
  `updated_at` datetime DEFAULT NULL,\
  PRIMARY KEY (`id`)\
) ENGINE=InnoDB DEFAULT CHARSET=utf8"

_keyword_candidates_table= \
   "DROP TABLE IF EXISTS keyword_candidates; CREATE TABLE IF NOT EXISTS `keyword_candidates` (\
  `id` int(11) NOT NULL AUTO_INCREMENT,\
  `keyword` varchar(20) DEFAULT NULL,\
  `created_at` datetime DEFAULT NULL,\
  `updated_at` datetime DEFAULT NULL,\
  PRIMARY KEY (`id`)\
) ENGINE=InnoDB DEFAULT CHARSET=utf8"

_insert_to_keyword_components =\
    "insert into keyword_components ({0}, created_at, updated_at) values ({1},CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);"

_get_unique_keys =\
    "select distinct type from keyword_components;"

def insert_to_keyword_components(values, fields, cursor):
    component_values = "', '".join(values)
    field_names = ", ".join(fields)

    sql_insert = _insert_to_keyword_components.format(field_names, "'" + component_values + "'")
    print(sql_insert)
    cursor.execute(sql_insert)

def insert_file(f, mode, cursor): 
    file_object = open(f, mode)
    for idx, line in enumerate(file_object): 
        values = line.strip().split(',')
        if idx == 0:
            fields = values 
        else:
            insert_to_keyword_components(values, fields, cursor)
   
def set_variables(carsCur,keys):
        obj = {}

        for key in keys:
            if LANGUAGE == 'both':
                carsCur.execute("select * from keyword_components where type = '{0}';".format(key))
            else:
                carsCur.execute("select * from keyword_components where type = '{0}' and language = '{1}';".format(key, LANGUAGE))
            obj[key] = carsCur.fetchall()
        return obj
    
def print_combo(ha, order, components, params_string, keyword_file=None):
   MATCH_TYPE = '+' 
   if None not in ha.values():
     line = "+" + " +".join([ ha[i] for i in order])+ "," + params_string
     if keyword_file == None:
         print(line)
     else:
         keyword_file.write(line + "\n")
   else:
     empty_index = ha.values().index(None)
     empty_key = ha.keys()[empty_index]
     #print("Serving Empty Key for {}".format(empty_key))
     for item in components[empty_key]:
         #if (empty_key == 'make_models'): pdb.set_trace()
         add_on = "&" + empty_key + '=' + item[1] + "&language=" + item[3]
         ha[empty_key] = item[4]
         print_combo(ha, order, components, params_string + add_on, keyword_file)
     ha[empty_key] = None

def build_pattern_hash(order, keys):
    h = {}
    for i in order:
        if i in keys:
            h[i] = None
        else:
            h[i] = i
    return h

# Simple routine to run a query on a database and print the results:
def run( carsConn ) :
    if options.file:
        keyword_file = open(options.file, "w") 
    else:
        keyword_file = None

    carsCur = carsConn.cursor()

    if options.rebuild:
        carsCur.execute(_drop_keyword_components)
        carsCur.execute(_keyword_components_table) 
        insert_file('./keyword_generator/keyword_list.csv','r', carsCur)    
        carsConn.commit()
    if True:
        carsCur.execute(_get_unique_keys)
        keyTuple = carsCur.fetchall()
        keys = [kt[0] for kt in keyTuple]
        components = set_variables(carsCur, keys)
        orders = [
                    ['kind_of_cars','car_nouns'],
                    ['style_of_cars','car_nouns'],
                    ['car_nouns','fuel_types'],
                    ['car_nouns','with','features'],
                    ['car_nouns','dealership_nouns'],
                    ['2018','makes'],
                    ['2018','makes','kind_of_cars'],
                    ['2019','make_models'],
                    ['2019','make_models','kind_of_cars']
                 ]
        
                    #['adjectives', 'kind_of_cars','auto'],
                    #['adjectives', 'style_of_cars','auto'],
                    #['adjectives', 'kind_of_cars','auto','fuel_types'],
                    #['adjectives', 'style_of_cars','auto','fuel_types'],
                    #['verbs','adjectives','makes','with','features'],
                    #['verbs','adjectives','make_models','with','features'],
                    #['verbs','adjectives','style_of_cars','nouns'],
        for order in orders:
            pattern = build_pattern_hash(order, keys)
            params_string = '' 
            print_combo(pattern, order, components, params_string, keyword_file)

if __name__ == "__main__":
    with Conn('sdp1') as c:
        carsdb = c.open_db('opt')   
        run(carsdb)
        carsdb.commit()
