from bingads.service_client import ServiceClient
from bingads.authorization import *
from bingads.v12.bulk import *
#from auth_helper import *
import pdb

import sys
import webbrowser
from time import gmtime, strftime
from suds import WebFault

# Optionally you can include logging to output traffic, for example the SOAP request and response.

import logging
logging.basicConfig(level=logging.INFO)
logging.getLogger('suds.client').setLevel(logging.DEBUG)
logging.getLogger('suds.transport.http').setLevel(logging.DEBUG)

if __name__ == '__main__':
    print("Python loads the web service proxies at runtime, so you will observe " \
          "a performance delay between program launch and main execution...\n")

    #ENVIRONMENT = 'production'
    #CLIENT_ID = '020803cb-6117-4846-8204-b7410fbcb78c'
    #DEVELOPER_TOKEN = 'BBD37VB98' # Universal token for sandbox
    #DEVELOPER_TOKEN = '119ZI19D52225479'

    authorization_data=AuthorizationData(
        account_id=None,
        customer_id=None,
        developer_token=DEVELOPER_TOKEN,
        authentication=None,
    )

    # It is recommended that you specify a non guessable 'state' request parameter to help prevent
    # cross site request forgery (CSRF). 
    CLIENT_STATE="ClientStateGoesHere"

    # The directory for the bulk files.
    FILE_DIRECTORY='./bulk/'

    # The name of the bulk upload result file.
    RESULT_FILE_NAME='result.csv'
    
    bulk_service_manager=BulkServiceManager(
        authorization_data=authorization_data, 
        poll_interval_in_milliseconds=5000, 
        environment=ENVIRONMENT,
    )

    campaign_service=ServiceClient(
        service='CampaignManagementService', 
        version=12,
        authorization_data=authorization_data, 
        environment=ENVIRONMENT,
    )

    customer_service=ServiceClient(
        'CustomerManagementService', 
        version=12,
        authorization_data=authorization_data, 
        environment=ENVIRONMENT,
    )
    
def authenticate_with_oauth():
    ''' 
    Sets the authentication property of the global AuthorizationData instance with OAuthDesktopMobileAuthCodeGrant.
    '''
    global authorization_data

    # If you already have both an access token and refresh token,
    # then you can construct OAuthDesktopMobileAuthCodeGrant with OAuthTokens.
    authentication=OAuthDesktopMobileAuthCodeGrant(
        client_id=CLIENT_ID,
        env=ENVIRONMENT,
        oauth_tokens=OAuthTokens(access_token=None,access_token_expires_in_seconds=0,refresh_token=None)
    )

    # It is recommended that you specify a non guessable 'state' request parameter to help prevent
    # cross site request forgery (CSRF). 
    authentication.state=CLIENT_STATE

    # Assign this authentication instance to the global authorization_data. 
    authorization_data.authentication=authentication   

    # Register the callback function to automatically save the refresh token anytime it is refreshed.
    # Uncomment this line if you want to store your refresh token. Be sure to save your refresh token securely.
    authorization_data.authentication.token_refreshed_callback=save_refresh_token
    
    refresh_token=get_refresh_token()
    
    try:
        # If we have a refresh token let's refresh it
        if refresh_token is not None:
            authorization_data.authentication.request_oauth_tokens_by_refresh_token(refresh_token)
        else:
            request_user_consent()
    except OAuthTokenRequestException:
        # The user could not be authenticated or the grant is expired. 
        # The user must first sign in and if needed grant the client application access to the requested scope.
        request_user_consent()
    
def request_user_consent():
    global authorization_data

    webbrowser.open(authorization_data.authentication.get_authorization_endpoint(), new=1)
    # For Python 3.x use 'input' instead of 'raw_input'
    if(sys.version_info.major >= 3):
        response_uri=input(
            "You need to provide consent for the application to access your Bing Ads accounts. " \
            "After you have granted consent in the web browser for the application to access your Bing Ads accounts, " \
            "please enter the response URI that includes the authorization 'code' parameter: \n"
        )
    else:
        response_uri=raw_input(
            "You need to provide consent for the application to access your Bing Ads accounts. " \
            "After you have granted consent in the web browser for the application to access your Bing Ads accounts, " \
            "please enter the response URI that includes the authorization 'code' parameter: \n"
        )

    if authorization_data.authentication.state != CLIENT_STATE:
       raise Exception("The OAuth response state does not match the client request state.")

    # Request access and refresh tokens using the URI that you provided manually during program execution.
    authorization_data.authentication.request_oauth_tokens_by_response_uri(response_uri=response_uri) 

def get_refresh_token():
    ''' 
    Returns a refresh token if stored locally.
    '''
    file=None
    try:
        file=open("refresh.txt")
        line=file.readline()
        file.close()
        return line if line else None
    except IOError:
        if file:
            file.close()
        return None

def save_refresh_token(oauth_tokens):
    ''' 
    Stores a refresh token locally. Be sure to save your refresh token securely.
    '''
    with open("refresh.txt","w+") as file:
        file.write(oauth_tokens.refresh_token)
        file.close()
    return None

def search_accounts_by_user_id(user_id):
    ''' 
    Search for account details by UserId.
    
    :param user_id: The Bing Ads user identifier.
    :type user_id: long
    :return: List of accounts that the user can manage.
    :rtype: Dictionary of AdvertiserAccount
    '''

    predicates={
        'Predicate': [
            {
                'Field': 'UserId',
                'Operator': 'Equals',
                'Value': user_id,
            },
        ]
    }

    accounts=[]

    page_index = 0
    PAGE_SIZE=100
    found_last_page = False

    while (not found_last_page):
        paging=set_elements_to_none(customer_service.factory.create('ns5:Paging'))
        paging.Index=page_index
        paging.Size=PAGE_SIZE
        search_accounts_response = customer_service.SearchAccounts(
            PageInfo=paging,
            Predicates=predicates
        )
        
        if search_accounts_response is not None and hasattr(search_accounts_response, 'AdvertiserAccount'):
            accounts.extend(search_accounts_response['AdvertiserAccount'])
            found_last_page = PAGE_SIZE > len(search_accounts_response['AdvertiserAccount'])
            page_index += 1
        else:
            found_last_page=True
    
    return {
        'AdvertiserAccount': accounts
    }

def set_elements_to_none(suds_object):
    for (element) in suds_object:
        suds_object.__setitem__(element[0], None)
    return suds_object

def output_status_message(message):
    print(message)

def output_bing_ads_webfault_error(error):
    if hasattr(error, 'ErrorCode'):
        output_status_message("ErrorCode: {0}".format(error.ErrorCode))
    if hasattr(error, 'Code'):
        output_status_message("Code: {0}".format(error.Code))
    if hasattr(error, 'Details'):
        output_status_message("Details: {0}".format(error.Details))
    if hasattr(error, 'FieldPath'):
        output_status_message("FieldPath: {0}".format(error.FieldPath))
    if hasattr(error, 'Message'):
        output_status_message("Message: {0}".format(error.Message))
    output_status_message('')

def output_webfault_errors(ex):
    if not hasattr(ex.fault, "detail"):
        raise Exception("Unknown WebFault")

    error_attribute_sets = (
        ["ApiFault", "OperationErrors", "OperationError"],
        ["AdApiFaultDetail", "Errors", "AdApiError"],
        ["ApiFaultDetail", "BatchErrors", "BatchError"],
        ["ApiFaultDetail", "OperationErrors", "OperationError"],
        ["EditorialApiFaultDetail", "BatchErrors", "BatchError"],
        ["EditorialApiFaultDetail", "EditorialErrors", "EditorialError"],
        ["EditorialApiFaultDetail", "OperationErrors", "OperationError"],
    )

    for error_attribute_set in error_attribute_sets:
        if output_error_detail(ex.fault.detail, error_attribute_set):
            return

    # Handle serialization errors, for example: The formatter threw an exception while trying to deserialize the message: 
    # There was an error while trying to deserialize parameter https://bingads.microsoft.com/CampaignManagement/v12:Entities.
    if hasattr(ex.fault, 'detail') \
        and hasattr(ex.fault.detail, 'ExceptionDetail'):
        api_errors=ex.fault.detail.ExceptionDetail
        if isinstance(api_errors, list):
            for api_error in api_errors:
                output_status_message(api_error.Message)
        else:
            output_status_message(api_errors.Message)
        return
    
    raise Exception("Unknown WebFault")

def output_error_detail(error_detail, error_attribute_set):
    api_errors = error_detail
    for field in error_attribute_set:
        api_errors = getattr(api_errors, field, None)
    if api_errors is None:
        return False
    if isinstance(api_errors, list):
        for api_error in api_errors:
            output_bing_ads_webfault_error(api_error)
    else:
        output_bing_ads_webfault_error(api_errors)
    return True

def output_bulk_campaigns(bulk_entities):
    for entity in bulk_entities:
        output_status_message("BulkCampaign: \n")
        output_status_message("Campaign Name: {0}".format(entity.campaign.Name))
        output_status_message("Campaign Id: {0}".format(entity.campaign.Id))

        if entity.has_errors:
            output_bulk_errors(entity.errors)

        output_status_message('')

def output_bulk_errors(errors):
    for error in errors:
        if error.error is not None:
            output_status_message("Number: {0}".format(error.error))
        output_status_message("Error: {0}".format(error.number))
        if error.editorial_reason_code is not None:
            output_status_message("EditorialTerm: {0}".format(error.editorial_term))
            output_status_message("EditorialReasonCode: {0}".format(error.editorial_reason_code))
            output_status_message("EditorialLocation: {0}".format(error.editorial_location))
            output_status_message("PublisherCountries: {0}".format(error.publisher_countries))
        output_status_message('')

# Main execution
if __name__ == '__main__':

    try:
        # You should authenticate for Bing Ads services with a Microsoft Account.
        authenticate_with_oauth()
        
        # Set to an empty user identifier to get the current authenticated Bing Ads user,
        # and then search for all accounts the user can access.
        user=customer_service.GetUser(None).User
        accounts=search_accounts_by_user_id(user.Id)

        # For this example we'll use the first account.
        authorization_data.account_id=accounts['AdvertiserAccount'][0].Id
        authorization_data.customer_id=accounts['AdvertiserAccount'][0].ParentCustomerId
            
        #To discover all SOAP elements accessible for each service, you can print the soap client.
        #For example, print(campaign_service.soap_client) will return Campaign, AdGroup, ExpandedTextAd, Keyword, etc. 

        #You could use the Campaign Management ServiceClient to add a Campaign as follows:
        #add_campaigns_response=campaign_service.AddCampaigns(
        #    AccountId=authorization_data.account_id,
        #    Campaigns=campaigns
        #)

        #bulkEntity-derived classes can also contain the SOAP objects, for example BulkCampaign can contain a Campaign.
        #As shown below, you can use the BulkServiceManager to upload a BulkCampaign. 
        #You should take advantage of the Bulk service to efficiently manage ads and keywords for all campaigns in an account.
        
        CAMPAIGN_ID_KEY=-123

        bulk_campaign=BulkCampaign()
        
        #The client_id may be used to associate records in the bulk upload file with records in the results file. The value of this field  
        #is not used or stored by the server; it is simply copied from the uploaded record to the corresponding result record. 
        #Note: This bulk file Client Id is not related to an application Client Id for OAuth. 

        bulk_campaign.client_id='YourClientIdGoesHere'
        campaign=campaign_service.factory.create('Campaign')
        
        #pdb.set_trace()
        #adgroup = campaign_service.factory.create('Adgroup')
        #adgroup.AdRotation (optional)
        #adgroup.AudienceAdsBidAdjustment
        #adgroup.BiddingScheme
        #adgroup.CpcBid
        #adgroup.EndDate
        #adgroup.ForwardCompatibilityMap
        #adgroup.Id
        #adgroup.Language
        #adgroup.Name
        #adgroup.Network
        #adgroup.PrivacyStatus
        #adgroup.Settings
        #adgroup.StartDate
        #adgroup.Status
        #adgroup.TrackingUrlTemplate
        #adgroup.UrlCustomParameters
        #


        #keyword
        #Bid
        #BiddingScheme
        #DestinationUrl
        #EditorialStatus
        #FinalAppUrls
        #FinalMobileUrls
        #FinalUrls
        #ForwardCompatibilityMap
        #Id
        #MatchType
        #Param1
        #Param2
        #Param3
        #Status
        #Text
        #TrackingUrlTemplate
        #UrlCustomParameters


        #ExpandedTextAd
        #AdFormatPreference
        #DevicePreference
        #EditorialStatus
        #FinalAppUrls
        #FinalMobileUrls
        #FinalUrls
        #ForwardCompatibilityMap
        #Id
        #Status
        #TrackingUrlTemplate
        #Type
        #UrlCustomParameters
        #Domain
        #Path1
        #Path2
        #Text
        #TextPart2
        #TitlePart1
        #TitlePart2
        #TitlePart3

        #When using the Campaign Management service, the Id cannot be set. In the context of a BulkCampaign, the Id is optional  
        #and may be used as a negative reference key during bulk upload. For example the same negative reference key for the campaign Id  
        #will be used when adding new ad groups to this new campaign, or when associating ad extensions with the campaign. 

        campaign.Id=CAMPAIGN_ID_KEY
        campaign.Name="Summer Shoes " + strftime("%a, %d %b %Y %H:%M:%S +0000", gmtime())
        campaign.Description="Summer shoes line."
        campaign.BudgetType='DailyBudgetStandard'
        campaign.DailyBudget=50
        campaign.TimeZone='PacificTimeUSCanadaTijuana'
        campaign.Status='Paused'
        bulk_campaign.campaign=campaign

        bulk_campaigns=[ bulk_campaign ]
    
        entity_upload_parameters=EntityUploadParameters(
            result_file_directory=FILE_DIRECTORY,
            result_file_name=RESULT_FILE_NAME,
            entities=bulk_campaigns,
            overwrite_result_file=True,
            response_mode='ErrorsAndResults'
        )
    
        output_status_message("Adding a BulkCampaign...\n")
        bulk_entities=list(bulk_service_manager.upload_entities(entity_upload_parameters))
    
        # Output the upload result entities
        for entity in bulk_entities:
            if isinstance(entity, BulkCampaign):
                output_bulk_campaigns([entity])
    
        output_status_message("Program execution completed")

    except WebFault as ex:
        output_webfault_errors(ex)
    except Exception as ex:
        output_status_message(ex)
