from bingads.service_client import ServiceClient
from campaignmanagement_example_helper import *
from bingads.authorization import *
from bingads.v12.bulk import *
from auth_helper import *
import pdb

import sys
import webbrowser
from time import gmtime, strftime
from suds import WebFault

# Optionally you can include logging to output traffic, for example the SOAP request and response.

import logging
logging.basicConfig(level=logging.INFO)
logging.getLogger('suds.client').setLevel(logging.DEBUG)
logging.getLogger('suds.transport.http').setLevel(logging.DEBUG)

if __name__ == '__main__':
    print("Python loads the web service proxies at runtime, so you will observe " \
          "a performance delay between program launch and main execution...\n")

    authorization_data=AuthorizationData(
        account_id=None,
        customer_id=None,
        developer_token=DEVELOPER_TOKEN,
        authentication=None,
    )

    # It is recommended that you specify a non guessable 'state' request parameter to help prevent
    # cross site request forgery (CSRF). 
    CLIENT_STATE="ClientStateGoesHere"

    # The directory for the bulk files.
    FILE_DIRECTORY='./bulk/'

    # The name of the bulk upload result file.
    RESULT_FILE_NAME='result.csv'
    
    bulk_service_manager=BulkServiceManager(
        authorization_data=authorization_data, 
        poll_interval_in_milliseconds=5000, 
        environment=ENVIRONMENT,
    )

    campaign_service=ServiceClient(
        service='CampaignManagementService', 
        version=12,
        authorization_data=authorization_data, 
        environment=ENVIRONMENT,
    )

    customer_service=ServiceClient(
        'CustomerManagementService', 
        version=12,
        authorization_data=authorization_data, 
        environment=ENVIRONMENT,
    )

# Main execution
if __name__ == '__main__':

    try:
        # You should authenticate for Bing Ads services with a Microsoft Account.
        authenticate_with_oauth(authorization_data)
        
        # Set to an empty user identifier to get the current authenticated Bing Ads user,
        # and then search for all accounts the user can access.
        user=customer_service.GetUser(None).User
        accounts=search_accounts_by_user_id(customer_service, user.Id)

        # For this example we'll use the first account.
        authorization_data.account_id=accounts['AdvertiserAccount'][0].Id
        authorization_data.customer_id=accounts['AdvertiserAccount'][0].ParentCustomerId
            
        #To discover all SOAP elements accessible for each service, you can print the soap client.
        #For example, print(campaign_service.soap_client) will return Campaign, AdGroup, ExpandedTextAd, Keyword, etc. 

        #You could use the Campaign Management ServiceClient to add a Campaign as follows:
        #add_campaigns_response=campaign_service.AddCampaigns(
        #    AccountId=authorization_data.account_id,
        #    Campaigns=campaigns
        #)

        #bulkEntity-derived classes can also contain the SOAP objects, for example BulkCampaign can contain a Campaign.
        #As shown below, you can use the BulkServiceManager to upload a BulkCampaign. 
        #You should take advantage of the Bulk service to efficiently manage ads and keywords for all campaigns in an account.
        

        bulk_campaign=BulkCampaign()
        
        #The client_id may be used to associate records in the bulk upload file with records in the results file. The value of this field  
        #is not used or stored by the server; it is simply copied from the uploaded record to the corresponding result record. 
        #Note: This bulk file Client Id is not related to an application Client Id for OAuth. 

        bulk_campaign.client_id='YourClientIdGoesHere'
        
        #adgroup = campaign_service.factory.create('Adgroup')
        #adgroup.AdRotation (optional)
        #adgroup.AudienceAdsBidAdjustment
        #adgroup.BiddingScheme
        #adgroup.CpcBid
        #adgroup.EndDate
        #adgroup.ForwardCompatibilityMap
        #adgroup.Id
        #adgroup.Language
        #adgroup.Name
        #adgroup.Network
        #adgroup.PrivacyStatus
        #adgroup.Settings
        #adgroup.StartDate
        #adgroup.Status
        #adgroup.TrackingUrlTemplate
        #adgroup.UrlCustomParameters
        #


        #keyword
        #Bid
        #BiddingScheme
        #DestinationUrl
        #EditorialStatus
        #FinalAppUrls
        #FinalMobileUrls
        #FinalUrls
        #ForwardCompatibilityMap
        #Id
        #MatchType
        #Param1
        #Param2
        #Param3
        #Status
        #Text
        #TrackingUrlTemplate
        #UrlCustomParameters


        #ExpandedTextAd
        #AdFormatPreference
        #DevicePreference
        #EditorialStatus
        #FinalAppUrls
        #FinalMobileUrls
        #FinalUrls
        #ForwardCompatibilityMap
        #Id
        #Status
        #TrackingUrlTemplate
        #Type
        #UrlCustomParameters
        #Domain
        #Path1
        #Path2
        #Text
        #TextPart2
        #TitlePart1
        #TitlePart2
        #TitlePart3

        #When using the Campaign Management service, the Id cannot be set. In the context of a BulkCampaign, the Id is optional  
        #and may be used as a negative reference key during bulk upload. For example the same negative reference key for the campaign Id  
        #will be used when adding new ad groups to this new campaign, or when associating ad extensions with the campaign. 

        CAMPAIGN_ID_KEY=-123
        
        get_campaigns = campaign_service.GetCampaignsByAccountId(
            AccountId=authorization_data.account_id,
            CampaignType=ALL_CAMPAIGN_TYPES
        )
        
        def o2j(iterationObj):
            dictObj = {}
            try:
              for key in iterationObj.__keylist__:
                dictObj.update({key: o2j(iterationObj[key])})
              return dictObj
            except AttributeError:
                if isinstance(iterationObj,list):
                  arrayObj=[]
                  for idx, key in enumerate(iterationObj):
                    arrayObj.append(o2j(iterationObj[idx]))
                  return arrayObj
                else:
                  return iterationObj
        
        for campaign in get_campaigns['Campaign']:
            print(o2j(campaign))
        
        output_array_of_campaign(get_campaigns)

        #campaign=campaign_service.factory.create('Campaign')

        #campaign.Id=CAMPAIGN_ID_KEY
        #campaign.Name="Summer Shoes " + strftime("%a, %d %b %Y %H:%M:%S +0000", gmtime())
        #campaign.Description="Summer shoes line."
        #campaign.BudgetType='DailyBudgetStandard'
        #campaign.DailyBudget=50
        #campaign.TimeZone='PacificTimeUSCanadaTijuana'
        #campaign.Status='Paused'
        #bulk_campaign.campaign=campaign

        #bulk_campaigns=[ bulk_campaign ]
    
        #entity_upload_parameters=EntityUploadParameters(
        #    result_file_directory=FILE_DIRECTORY,
        #    result_file_name=RESULT_FILE_NAME,
        #    entities=bulk_campaigns,
        #    overwrite_result_file=True,
        #    response_mode='ErrorsAndResults'
        #)
    
        #output_status_message("Adding a BulkCampaign...\n")
        #bulk_entities=list(bulk_service_manager.upload_entities(entity_upload_parameters))
    
        ## Output the upload result entities
        #for entity in bulk_entities:
        #    if isinstance(entity, BulkCampaign):
        #        output_bulk_campaigns([entity])
    
        output_status_message("Program execution completed")

    except WebFault as ex:
        output_webfault_errors(ex)
    except Exception as ex:
        output_status_message(ex)
