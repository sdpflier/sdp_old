import sys
import os
import pdb
import datetime
sys.path.insert(0,os.environ['SDP_ROOT'])


from utils.OpenConn import DbConnection as Conn
from optparse import OptionParser

parser = OptionParser()
parser.add_option("-d", "--date", default=datetime.datetime.now().strftime("%Y-%m-%d"),
                  help="Date to run the report")
(options, args) = parser.parse_args()

def run( carsConn ) :
    carsCur = carsConn.cursor()

        
if __name__ == "__main__":
    with Conn('sdp1') as c:
        carsdb = c.open_db('konp')   
        run(carsdb)
        carsdb.commit()
