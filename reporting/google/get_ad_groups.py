#!/usr/bin/env python
"""
Modified on Thu Dec 6 08:43:44 2018

@author: jovanna
"""

import sdp_googleads as g


if __name__ == '__main__':
  # Initialize client object.
      adwords_client = g.adwords.AdWordsClient.LoadFromStorage('/Users/jovanna/repos/sdp/reporting/google/dev_googleads.yaml')
      campaigns = g.get_campaigns(adwords_client)
      ad_groups = g.get_ad_groups(adwords_client,campaigns)
