#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 26 11:16:20 2019

@author: jovanna
"""

from utils.OpenConn import DbConnection as Conn
import sdp_googleads as g

dealer = 'Pirola'

query_campaign_names = """
SELECT CONCAT('%s_',make_name)
FROM cars.clean_cars
where stock_dealer_code = %s
group by 1
;
"""

query_ad_group_names = """
select make_name,model__name
FROM cars.clean_cars
where model__name is not null and stock_dealer_code = %s
group by 1,2;
"""

#each word in data will be added to +make +model
#data = ['pronta consegna','incentivi','prezzo','nuova','2019',
               #           'costo','2018','listino','km 0','promozione','offerte',
               #           'km zero']
data = ['nuova','incentivi','prezzo','2019','km 0', 'promozioni','offerte']

#The following is ad content
"""This code adds the keywords based on the ad_groups and campaign names
                        'headlinePart1': ('%s %s' % (make.title(), model.title())),
                        'headlinePart2': Km 0 e Pronta Consegna, #max 25
                        'headlinePart3': AutoJab,
                        'description': CONTENT HERE, #max 90
                        'description2': "Prenota un test drive senza impegno sull'ultima %s" % model.title(),
                        'path1' : '%s' % make.title(),
                        'path2' : '%s' % model.title(),
                        'finalUrls': ['https://www.autojab.com/?make_name=%s&model_name=%s&news_links=false' % (make,model)],
                        'trackingUrlTemplate': ('{lpurl}&ts_id=1&geo_loc_id={loc_physical_ms}&network={network}&device={device}&keyword={keyword}&placement={placement}&ad_rank={adposition}&creative={creative}&gclid={gclid}')             
"""
#headlinePart2, headlinePart3, description, description2
content = []
content.append(['Risparmia sulle tutte le vetture %s pre-configurate','Visita il nostro sito per conoscere quali %s sono in stock nella tua zona'])
content.append(['Visita il nostro sito per conoscere quali vetture %s sono in stock nella tua zona','Richiedi informazioni su AutoJab per bloccare le offerte su tutte le %s'])
content.append(['Entra nel Mondo %s e scopri le offerte su tutti i modelli pre-configurati','Prenota un test drive senza impegno sull\' ultima %s'])

def process_campaigns(c):
    parts = c[0].split('_',1)
    dealer,make = parts
    return make,c[1]

def get_data(db,query,input):
    cursor = db.cursor()
    cursor.execute(query,input)
    m = cursor.fetchall()
    return m


if __name__ == '__main__':
    with Conn('sdp1') as c:
        conn = c.open_db('cars')   
        #client = g.adwords.AdWordsClient.LoadFromStorage('/Users/jovanna/repos/sdp/reporting/google/dev_googleads.yaml')  #dev client
        client = g.adwords_client #production client
        #campaign_names = get_data(conn,query_campaign_names,(dealer,dealer))
        #g.add_campaigns(client,campaign_names) #create the campaigns
        ad_group_names = get_data(conn,query_ad_group_names,dealer)
        c = g.get_campaigns(client)
        #make sure you pass to ad_groups only the campaigns you need to
        c = [s for s in c if dealer in s[0]]
        campaigns = [process_campaigns(item) for item in c]
        #print campaigns
        #g.add_adgroups(client,ad_group_names,campaigns) #create the ad groups
        ad_groups = g.get_ad_groups(client,campaigns)
        g.add_keywords(client,ad_groups,data)   #create the keywords
        g.add_expanded_text_ad(client,ad_groups,content)    #create the ads
        conn.commit()
    print('Done')   

