#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  7 12:28:51 2018

@author: jovanna
"""
from googleads import adwords
import os
import datetime
import uuid
import pdb

cwd = os.getcwd()
adwords_client = adwords.AdWordsClient.LoadFromStorage(cwd + '/googleads.yaml')

def add_campaigns(adwords_client,data):
      # Initialize appropriate services.
      campaign_service = adwords_client.GetService('CampaignService', version='v201809')
      budget_service = adwords_client.GetService('BudgetService', version='v201809')
    
      # Create a budget, which can be shared by multiple campaigns.
      budget = {
          'name': 'Budget #%s' % uuid.uuid4(),
          'amount': {
              'microAmount': '50000000'
          },
          'deliveryMethod': 'STANDARD'
      }
    
      budget_operations = [{
          'operator': 'ADD',
          'operand': budget
      }]
    #  match_type = ['generic','broad','exact','phrase','new','broadMOD']
    #  camp_name = [i+'_'+j for i in make for j in match_type]
    
    
      # Add the budget.
      budget_id = budget_service.mutate(budget_operations)['value'][0][
          'budgetId']
      # Construct operations and add campaigns.
      operations = []
      
      for k in data:
          campaign_operations = {
              'operator': 'ADD',
              'operand': {
                  'name': '%s' % k,
                  'status': 'PAUSED',
                  'advertisingChannelType': 'SEARCH',
                  'biddingStrategyConfiguration': {
                      'biddingStrategyType': 'TARGET_SPEND',
                  },
                  'endDate': (datetime.datetime.now() +
                              datetime.timedelta(40)).strftime('%Y%m%d'),
                  # Note that only the budgetId is required
                  'budget': {
                      'budgetId': budget_id
                  },
                  'networkSetting': {
                      'targetGoogleSearch': 'true',
                      'targetSearchNetwork': 'true',
                      'targetContentNetwork': 'false',
                      'targetPartnerSearchNetwork': 'false'
                  },
    #               'location': {
    #                  'xsi_type': 'Location',
    #                  'id': '2380'
    #              },
    #               'language': {
    #                  'xsi_type': 'Language',
    #                  'id': '1004'
    #              },
                    
                  # Optional fields
        #          'startDate': (datetime.datetime.now() +
        #                        datetime.timedelta(1)).strftime('%Y%m%d'),
        #          'frequencyCap': {
        #              'impressions': '5',
        #              'timeUnit': 'DAY',
        #              'level': 'ADGROUP'
        #          },
                  'settings': [
                      {
                          'xsi_type': 'GeoTargetTypeSetting',
                          'positiveGeoTargetType': 'LOCATION_OF_PRESENCE',
        #                  'negativeGeoTargetType': 'DONT_CARE'
                      }
                              ]
                  }}
          operations.append(campaign_operations)
      print operations
      campaigns = campaign_service.mutate(operations)
    
      # Display results.
      for campaign in campaigns['value']:
        print ('Campaign with name "%s" and id "%s" was added.'
               % (campaign['name'], campaign['id']))


def add_adgroups(adwords_client,data,campaigns):
  for c in campaigns:  
      campaign_name = c[0]
      campaign_id = c[1]
    # Initialize appropriate service.
      ad_group_service = adwords_client.GetService('AdGroupService', version='v201809')
      operations = []
      #print data
      gen = (k for k in data if campaign_name in k)
      for k in gen:
      # Construct operations and add ad groups.
          ad_group_operations = [{
              'operator': 'ADD',
              'operand': {
                  'campaignId': campaign_id,
                  'name': '%s' % '_'.join((k[0],k[1])),
                  'status': 'ENABLED',
                  'biddingStrategyConfiguration': {
                      'bids': [
                          {
                              'xsi_type': 'CpcBid',
                              'bid': {
                                  'microAmount': '1000000'
                              },
                          }]},
                  'settings': [
                      {
                          # Targeting restriction settings. Depending on the
                          # criterionTypeGroup value, most TargetingSettingDetail only
                          # affect Display campaigns. However, the
                          # USER_INTEREST_AND_LIST value works for RLSA campaigns -
                          # Search campaigns targeting using a remarketing list.
                          'xsi_type': 'TargetingSetting',
                          'details': [
                              # Restricting to serve ads that match your ad group
                              # placements. This is equivalent to choosing
                              # "Target and bid" in the UI.
                              {
                                  'xsi_type': 'TargetingSettingDetail',
                                  'criterionTypeGroup': 'PLACEMENT',
                                  'targetAll': 'false',
                              },
                              # Using your ad group verticals only for bidding. This is
                              # equivalent to choosing "Bid only" in the UI.
                              {
                                  'xsi_type': 'TargetingSettingDetail',
                                  'criterionTypeGroup': 'VERTICAL',
                                  'targetAll': 'true',
                              },]}]}}]
          operations.append(ad_group_operations)
     
          
      #print operations
      ad_groups = ad_group_service.mutate(operations)
    
      # Display results.
      for ad_group in ad_groups['value']:
        print ('Ad group with name "%s" and id "%s" was added.'
               % (ad_group['name'], ad_group['id']))
    
    
def get_campaigns(adwords_client):
      PAGE_SIZE = 100
      # Initialize appropriate service.
      campaign_service = adwords_client.GetService('CampaignService', version='v201809')
    
      # Construct selector and get all campaigns.
      offset = 0
      selector = {
          'fields': ['Id', 'Name', 'Status','Settings'],
          'paging': {
              'startIndex': str(offset),
              'numberResults': str(PAGE_SIZE)
          }
      }
    
      more_pages = True
      while more_pages:
        page = campaign_service.get(selector)
    
        # Display results.
        if 'entries' in page:
          campaigns = []
          for campaign in page['entries']:
            print ('Campaign with id "%s", name "%s", and status "%s"  was '
                   'found.' % (campaign['id'], campaign['name'],
                               campaign['status']))
            campaigns.append((campaign['name'],campaign['id']))
          
        else:
          print 'No campaigns were found.'
        offset += PAGE_SIZE
        selector['paging']['startIndex'] = str(offset)
        more_pages = offset < int(page['totalNumEntries'])
        print campaigns
        return campaigns
    

def get_ad_groups(adwords_client, campaigns):
      PAGE_SIZE = 500
      ad_groups =[]
      for c in campaigns:
          campaign_id = c[1]  
          
          # Initialize appropriate service.
          ad_group_service = adwords_client.GetService('AdGroupService', version='v201809')
        
          # Construct selector and get all ad groups.
          offset = 0
          selector = {
              'fields': ['Id', 'Name', 'Status'],
              'predicates': [
                  {
                      'field': 'CampaignId',
                      'operator': 'EQUALS',
                      'values': [campaign_id]
                  }
              ],
              'paging': {
                  'startIndex': str(offset),
                  'numberResults': str(PAGE_SIZE)
              }
          }
          more_pages = True
          while more_pages:
            page = ad_group_service.get(selector)
        
            # Display results.
            if 'entries' in page:
              for ad_group in page['entries']:
                print ('Ad group with name "%s", id "%s" and status "%s" was '
                       'found.' % (ad_group['name'], ad_group['id'],
                                   ad_group['status']))
                ad_groups.append((ad_group['name'],ad_group['id']))
            else:
              print 'No ad groups were found.'
            offset += PAGE_SIZE
            selector['paging']['startIndex'] = str(offset)
            more_pages = offset < int(page['totalNumEntries'])
      #if ad_groups:
      #    print ad_groups
      #else:
      #    print "No ad groups found!"
      return ad_groups

def add_keywords(client, ad_groups, words):
      PAGE_SIZE = 500
  # Initialize appropriate service.
      ad_group_criterion_service = client.GetService(
              'AdGroupCriterionService', version='v201809')
      for ad in ad_groups:
          operations = []
          ad_group_name,ad_group_id = ad
          parts = ad_group_name.split('_',1)
          make,model = parts
          #model_parts = model.split('-',1)
          for word in words:
              # Construct keyword ad group criterion object.
                  keyword = {
                      'xsi_type': 'BiddableAdGroupCriterion',
                      'adGroupId': ad_group_id,
                      'criterion': {
                          'xsi_type': 'Keyword',
                          'matchType': 'BROAD',
                          'text': '+%s +%s %s' % (make, model, word)
                          },
      # These fields are optional.
                  'userStatus': 'PAUSED',
      #            'finalUrls': {
       #           'urls': ['http://example.com/mars']
        #                  }
                      }
  # Construct operations and add ad group criteria.
                  keyword_operations = [{
                      'operator': 'ADD',
                      'operand': keyword
                      }]
                  operations.append(keyword_operations)
          ad_group_criteria = ad_group_criterion_service.mutate(operations)['value']

  # Display results.
          for criterion in ad_group_criteria:
              print ('Keyword ad group criterion with ad group id "%s", criterion id '
                                 '"%s", text "%s", and match type "%s" was added.'
                                 % (criterion['adGroupId'], criterion['criterion']['id'],
                                    criterion['criterion']['text'],
                                    criterion['criterion']['matchType']))


if __name__ == '__main__':
          adwords_client = adwords.AdWordsClient.LoadFromStorage('/Users/jovanna/repos/sdp/reporting/google/dev_googleads.yaml')
          #add_campaigns(adwords_client,['Eberjan'])
          #add_adgroups(adwords_client,data,campaigns)