#!/usr/bin/env python
"""
Modified on Thu Dec 6 08:43:44 2018

@author: jovanna
"""

import sdp_googleads as g


if __name__ == '__main__':
    adwords_client = g.adwords.AdWordsClient.LoadFromStorage('/Users/jovanna/repos/sdp/reporting/google/dev_googleads.yaml')
    g.get_campaigns(adwords_client)
