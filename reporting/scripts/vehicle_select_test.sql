select 
  ap.value, 
  f.name, 
  count(*) 
from arrivals a 
join arrival_properties ap 
on a.id = ap.arrival_id 
left join forms f 
on a.id = f.arrival_id 
where ap.name = 'vehicle_vs_dealer' 
group by 1,2;
