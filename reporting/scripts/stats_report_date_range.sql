select 
date, 
source, 
MAX(CASE WHEN name = 'keywords' THEN count ELSE 0 END) AS keywords,
MAX(CASE WHEN name = 'impressions' THEN count ELSE 0 END) AS impressions,
MAX(CASE WHEN name = 'clicks' THEN count ELSE 0 END) AS clicks,
#MAX(CASE WHEN TRUE THEN clicks/impressions ELSE 0 END) AS CTR,
MAX(CASE WHEN name = 'arrivals' THEN count ELSE 0 END) AS arrivals,
MAX(CASE WHEN name = 'index' THEN count ELSE 0 END) AS 'index',
MAX(CASE WHEN name = 'poll_success' THEN count ELSE 0 END) AS poll_success,
MAX(CASE WHEN name = 'vehicle-description' THEN count ELSE 0 END) AS vehicle_description 
from (
	select date(f.created_at) date,
	IF(a.arrival_url LIKE '%gclid%', 'Google', IF(a.arrival_url LIKE '%msclkid%', 'Bing', 'Unknown')) source,
	f.name,
	count(*) count
	from forms f
	join arrivals a
	on f.arrival_id = a.id group by 1,2,3
	union
	select date(a.created_at) date,
	IF(a.arrival_url LIKE '%gclid%', 'Google', IF(a.arrival_url LIKE '%msclkid%', 'Bing', 'Unknown')) source,
	'arrivals',
	count(*) count
	from arrivals a
	where a.user_agent not like '%Bot%'
		and a.arrival_url like '%?m%' group by 1,2,3
	union
	select date, source, name, value as 'count' from traffic_data
) 
a where date(date) between '{0}' and '{1}' group by 1,2 order by 1;
