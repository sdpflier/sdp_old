# Find Google Devices

select arrival_url like '%device=m%' device, user_agent from arrivals a where created_at > '2018-12-17' and arrival_url like '%autojab.com/?%' and user_agent not like '%bot%' and arrival_url like '%gclid%';


#Bing Arrivals
select * from konfigurator_production.arrivals a where created_at > '2018-12-17' and arrival_url like '%autojab.com/?%' and user_agent not like '%bot%' and arrival_url not like '%msclkid%';


create temporary table google_arrivals select id from konfigurator_production.arrivals a where created_at > '2018-12-17' and arrival_url like '%autojab.com/?%' and user_agent not like '%bot%' and arrival_url like '%gclid%' and arrival_url not like '%device=m%';
create temporary table bing_arrivals select * from konfigurator_production.arrivals a where created_at > '2018-12-17' and arrival_url like '%autojab.com/?%' and user_agent not like '%bot%' and arrival_url not like '%msclkid%';
create temporary table cap_of_arrivals select json_extract(data, '$.cap') cap from forms f join (select id from bing_arrivals) ba on f.arrival_id = ba.id where name = 'index';
create temporary table comuni_arrivals select cc.* from facts.cap_comuni cc join cap_of_arrivals coa on cc.cap = coa.cap;
select concat(latitudine_g1, '.',latitudine_g2,',',longitudine_g1,'.',longitudine_g2) from facts.comuni c join comuni_arrivals ca on c.comune = ca.comuni;

