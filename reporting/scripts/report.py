import sys
import os
import pdb
import datetime
sys.path.insert(0,os.environ['SDP_ROOT'])


from utils.OpenConn import DbConnection as Conn
from optparse import OptionParser

parser = OptionParser()
parser.add_option("-d", "--date", default=datetime.datetime.now().strftime("%Y-%m-%d"),
                  help="Date to run the report")
parser.add_option("-s", "--startdate", default=datetime.datetime.now().strftime("%Y-%m-%d"),
                  help="Date to run the report")
parser.add_option("-e", "--enddate", default=datetime.datetime.now().strftime("%Y-%m-%d"),
                  help="Date to run the report")
parser.add_option("-i", "--inputfile",
                  help="File containing report sql. Default stats_report.sql")
(options, args) = parser.parse_args()

#this should be linked to the sql query somehow
columns = ('date', 'source', 'impressions', 'clicks', 'cost', 'arrivals', 'index', 'poll_success', 'vehicle_description')

def default_report(opts): 
    pathname = os.path.dirname(sys.argv[0])        
    if opts.inputfile:
        return opts.inputfile
    else:
        return os.path.abspath(pathname) + '/stats_report_date_range.sql'

def run( carsConn ) :
    carsCur = carsConn.cursor()
    inputfile = open(default_report(options), 'r')
    sql = inputfile.read().format(options.startdate, options.enddate)
    carsCur.execute(sql)
    
    rows = carsCur.fetchall()
    rows = (columns,) + rows
    for row in rows:
        out = ''
        for column in row:
            if type(column) == type('str'):
                out += column.rjust(12).ljust(14)
            else:
                out += column.strftime("%Y-%m-%d").rjust(12).ljust(14)
        print(out)

        
if __name__ == "__main__":
    with Conn('sdp1') as c:
        carsdb = c.open_db('konp')   
        run(carsdb)
        carsdb.commit()
