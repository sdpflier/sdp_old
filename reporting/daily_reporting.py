#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 21 08:43:44 2018

@author: jovanna
"""
from utils.OpenConn import DbConnection as Conn
from utils.mail import Email
import utils.prettytable as p

query1 = """
SELECT date,source,MAX(CASE WHEN name = 'arrivals' THEN count ELSE 0 END) AS arrivals,
MAX(CASE WHEN name='index' THEN count ELSE 0  END) AS 'index',MAX(CASE WHEN name = 
'poll_success' THEN count ELSE 0 END) AS poll_success,MAX(CASE WHEN name = 'vehicle-description' 
THEN count ELSE 0 END) AS vehicle_description FROM (SELECT DATE(f.created_at) date, 
IF(a.arrival_url LIKE '%gclid%','Google', IF(a.arrival_url LIKE '%msclkid%','Bing',
'Organic')) source,f.name,COUNT(*) count FROM forms f JOIN arrivals a ON 
f.arrival_id = a.id where f.data not like '%test%' GROUP BY 1 , 2 , 3 UNION SELECT DATE(a.created_at) date,     
IF(a.arrival_url LIKE '%gclid%','Google', IF(a.arrival_url LIKE '%msclkid%','Bing',
'Organic')) source, 'arrivals',COUNT(*) count FROM arrivals a WHERE a.user_agent NOT 
LIKE '%Bot%' AND a.arrival_url LIKE '%?m%' GROUP BY 1,2,3) a GROUP BY 1,2 ORDER BY 1
desc limit 18;
"""

query2 = """
SELECT 
    ap.value as vehicle_select, f.name, COUNT(*)
FROM
    arrivals a
        JOIN
    arrival_properties ap ON a.id = ap.arrival_id
        LEFT JOIN
    forms f ON a.id = f.arrival_id
WHERE
    ap.name = 'vehicle_vs_dealer' and data not like '%test%'
GROUP BY 1 , 2;
"""

queries = [query1, query2]


def display_sql(cursor,title,header,padd):
	col_names = [cn[0].upper() for cn in cursor.description]
	rows = cursor.fetchall()
	
	sqlr = p.PrettyTable()
	sqlr.header = header
	sqlr.padding_width = padd
	sqlr.hrules = p.ALL
	sqlr.vrules = p.ALL
#	sqlr.title = title
	row_id = -1
	
	for name in col_names:
		row_id += 1
		sqlr.add_column(name, [row[row_id] for row in rows])
	
	return sqlr.get_html_string()

def pull_report(db):
    text = ""
    for query in queries:
        cursor = db.cursor()
        cursor.execute(query)
        data = display_sql(cursor,"title",True,1)
        text += data
    return text

if __name__ == "__main__":
    with Conn('sdp1') as c:
        conn = c.open_db('konp')   
        message_text = pull_report(conn)
        conn.commit()

    #recipients=("reporting@groupsdp.com","Verica.Lukic@evolvea.rs")
    m = Email()
    mFrom = "SDP Report <notifications@groupsdp.com>"
    m.setFrom(mFrom)
    m.setSubject("[Report] Daily user activity")
    m.addRecipient("jovanna.selvaggi@groupsdp.com")
    #m.addRecipient("reporting@groupsdp.com")
    #for recipient in recipients:
       # m.addRecipient(recipient)
    m.setHtmlBody(message_text)
    m.send()
    print('Task completed')   
