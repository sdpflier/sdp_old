#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
"""
Created on Sat Nov  3 20:35:56 2018

@author: jovanna
"""

import feedparser
import datetime
from utils.OpenConn import DbConnection as Conn

query_tags = """
Select make_name, model__name from cars.clean_cars group by 1,2
"""

SQL_feeds = """
SELECT * from cars.rss_feeds WHERE title=%s AND created_at=%s
"""
SQL_load = """
INSERT INTO cars.rss_feeds VALUES (%s,%s,%s,%s,%s,%s)
"""
content = 'https://www.quattroruote.it/content/quattroruote/it/listino/feeds/newsRss/feed.xml'

language = 'IT'

def get_tags(db):
    cursor = db.cursor()
    cursor.execute(query_tags)
    data = cursor.fetchall()
    #flat the tuple
    t = [e for l in data for e in l]
    t_list = list(set(t))
    #removes the null strings
    if 'NULL' in t_list: t_list.remove('NULL')
    return t_list

def check_db(title,date,cursor):
    cursor.execute(SQL_feeds,(title,date))
    if not cursor.fetchall():
        return True
    else:
        return False

def pull_rss(db):
    #parse the rss feed
    f = feedparser.parse(content)
    tags = get_tags(db)
    cursor = db.cursor()
    #print(tags)
    for article in f['entries']:
        title = article.title
        description = article.summary[0:499]
        link = article.link
        date = datetime.datetime.strptime(article.published, "%a, %d %b %Y %H:%M:%S %Z").strftime("%Y-%m-%d")
        #gets all the tags in the feed
        matches=[word for word in tags if word in description]
        keys = ','.join(matches)
        print matches
        #check the feed is not in the db and load it up
        #if check_db(title,date,cursor):
            #cursor.execute(SQL_load,(title,description,keys,language,link,date))

if __name__ == "__main__":
    with Conn('sdp1') as c:
        conn = c.open_db('opt')   
        pull_rss(conn)
        conn.commit()
    print('Database updated')   
