#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 19 16:55:04 2018

@author: jovanna
"""

import requests
from mechanize import Browser
import json
import simplejson
from dateutil import parser
from sdp.utils.OpenConn import DbConnection as Conn
import re

query = "replace into cars.google_news (title,content,url,source,created_at) values (%s,%s,%s,%s,%s)"
urls = ("https://news.google.com/news/rss/search/section/q/auto+notizie?ned=it&gl=IT&hl=it","https://news.google.com/news/rss/search/section/q/auto+incentivi?ned=it&gl=IT&hl=it","https://news.google.com/news/rss/search/section/q/auto+vendite?ned=it&gl=IT&hl=it",)

def strip_tags(value):
    """Returns the given HTML with all tags stripped."""
    return re.sub(r'<[^>]*?>', '', value.encode("utf-8"))

def get_content(urls,db):
    br = Browser()
    #br.set_all_readonly(False)    
    br.set_handle_robots(False)   #ignore robots
    br.set_handle_refresh(False)  
    br.addheaders = [('User-agent', 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.1) Gecko/2008071615 Fedora/3.0.1-1.fc9 Firefox/3.0.1')]

    #cursor = db.cursor()
    for url in urls:
        r = requests.get(url)
        r.status_code
        print(r.headers['content-type'])
        #r.json()
        #r = br.open(url)
        #j = simplejson.load(r)
        j = r.json()
        print j    
        status = j.get('responseStatus')
        if status != 200:
            print "Request failed. Status :", status
            continue 
    
        for result in j.get('responseData', {}).get('results', []):
            title =  result['title']
            content =  result['content']
            published_date = parser.parse(result['publishedDate'])
            url = result['unescapedUrl']
            source = result['publisher']
            args = (strip_tags(title),strip_tags(content),url,source,published_date) 
            #cursor.execute(query,args)
            print(args)

if __name__ == "__main__":
    with Conn('sdp1') as c:
        conn = c.open_db('cars')
        get_content(urls,conn)
        conn.commit()
    print('Database updated')   
